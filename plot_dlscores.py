import sys 
import os; 
import matplotlib
matplotlib.use('Agg') 
from collections import OrderedDict, namedtuple, Counter;
import numpy as np;
sys.path.insert(0,"/users/avanti/caffe/python/")
#Import some general util stuff
scriptsDir = os.environ.get("UTIL_SCRIPTS_DIR");
if (scriptsDir is None):
    raise Exception("Please set environment variable UTIL_SCRIPTS_DIR to point to av_scripts");
sys.path.insert(0,scriptsDir);

scriptsDir = os.environ.get("ENHANCER_SCRIPTS_DIR");
if (scriptsDir is None):
    raise Exception("Please set environment variable ENHANCER_SCRIPTS_DIR to point to enhancer_prediction_code");
sys.path.insert(0,scriptsDir+"/featureSelector/deepLIFFT/");
import deepLIFTutils
scriptsDir = os.environ.get("KERAS_DIR");
if (scriptsDir is None):
    raise Exception("Please set environment variable KERAS_DIR");
import glob
import os,sys 
import compare_filters_to_known_motifs
from importDataPackage import importData
import pathSetter;
import util;
import fileProcessing as fp
import keras
from plottingUtilitiesPackage import matplotlibHelpers as mplh;
import criticalSubsetIdentification as csi

sys.path.insert(0,scriptsDir)
sys.path.insert(0,scriptsDir+"/featureSelector/deepLIFFT/kerasBasedBackprop");
from deepLIFTonGPU import ScoreTypes, Activations_enum, OutLayerInfo, getScoreFunc
np.random.seed(1234)
###################################################################################
import pysam 
ref_source = pysam.FastaFile("hg19.genome.fa")
chrom=sys.argv[0] 
startpos=int(sys.argv[1]) 
endpos=int(sys.argv[2]) 
seq=ref_source.fetch(chrom,startpos,endpos) 
seq=seq.upper() 
bg=open(sys.argv[3],'r').read().strip().split('\n') 

hits=dict() 
for line in bg: 
    tokens=line.split('\t') 
    curpos=int(tokens[1]) 
    if curpos >= startpos: 
        if curpos <= endpos: 
            hits[curpos]=float(tokens[-1]) 

print "indexed!" 
print "building input matrix" 

seq_index=dict() 
#ACGT
seq_index['A']=0
seq_index['C']=1
seq_index['G']=2 
seq_index['T']=3  

#get a matrix of deepLIFT scores 
r=endpos-startpos+1
c=4 
dl_scores=np.zeros((r,c))
cur_row=0 
for pos in range(startpos,endpos+1): 
    seqoffset=pos-startpos
    curbase=seq[seqoffset] 
    if pos in hits: 
        curdl=hits[pos] 
    else: 
        curdl=0 
    dl_scores[seq_index[curbase]][seqoffset]=curdl 
print "plotting!" 
deepLIFTutils.plotWeights(dl_scores,0,"",figSize=(r,4),outputFile=str(chrom)+"_"+str(startpos)+"_"+str(endpos)+".png")

