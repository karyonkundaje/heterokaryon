import sys 
import re 

def parse_arguments(): 
    inputfile=sys.argv[1] 
    outputf=sys.argv[2] 
    threshold=float(sys.argv[3]) 
    chrom=sys.argv[4]
    return inputfile,outputf,threshold,chrom

def main(): 
    counter=0
    curlines=0
    hist=dict() 
    inputfile,outputfile,threshold,chrom=parse_arguments() 
    data=open(inputfile,'r').read().strip().split('\n') 
    prog=re.compile('\-?\d*\.?\d+[e]?\-?\d*,\d') 
    splitter=re.compile(',') 
    outf=open(outputfile,'w') 
    outall="" 
    for line in data: 
        tokens=line.split('\t')
        score_tuples=[re.split(splitter,i) for i in re.findall(prog,tokens[3])]
        score_tuples=[[float(i[0]),int(i[1])] for i in score_tuples]
        use=False 
        outstring='\t'.join(tokens[0:3])+'\t'+"id:"+str(counter)+",qcat:["
        for s in score_tuples: 
            if abs(s[0])>threshold:
                s[0]=round(s[0],3) 
                if s[0] not in hist: 
                    hist[s[0]]=1 
                else: 
                    hist[s[0]]+=1 
                outstring+=str(s)
                use=True 
        if use: 
            outstring+="]\n"
            if curlines < 100000:
                outall+=outstring
            else: 
                outf.write(outall) 
                outall=outstring 
                curlines=0 
            curlines+=1 
            counter+=1 
    outf.write(outall) 
    outf_hist=open(outputfile+'.HISTOGRAM','w')
    for key in hist: 
        outf_hist.write(str(key)+'\t'+str(hist[key])+'\n')
    outf.close() 
    #####################################################
    data=open(outputfile,'r').read().strip().split('\n') 
    outf=open(outputfile+'.motif','w') 
    
    curpos=None 
    curmotif=[] 
    curscores=[]
    known=[] 
    firstpos=None 
    base_dict=dict() 
    base_dict['0']='A' 
    base_dict['1']='C' 
    base_dict['2']='G' 
    base_dict['3']='T' 

    maxgap=5 # maximum gap between bases with significant deepLIFT scores in order for them to be considered part of same motif 
    for line in data:
        #pdb.set_trace() 
        tokens=line.split('\t') 
        pos=int(tokens[1])
        baseval=tokens[3].split('[')[2].split(']')[0].replace(' ','').split(',') 
        score=baseval[0] 
        base=baseval[1] 
        if firstpos==None: 
            #first time we see a base! start first motif 
            firstpos=pos 
            curpos=pos 
            curmotif=[base_dict[base]] 
            known=[base_dict[base]]
            curscores=[score]
        elif (pos - curpos) > maxgap:
            #store current motif & reset! 
            if len(curmotif)>4: 
                if len(known)>4: 
                    mean_score=sum([float(i) for i in curscores])/len(curscores)            
                    outstring=chrom+'\t'+str(firstpos)+'\t'+str(curpos)+'\t'+str(round(mean_score,3))+'\t'+''.join(curmotif)+'\n'
                    outf.write(outstring)
            #reset everything! 
            curpos=pos 
            curmotif=[base_dict[base]] 
            known=[base_dict[base]]
            curscores=[score]
            firstpos=pos 
        elif (pos-curpos==1): 
            #no bases skipped, update current motif 
            curpos=pos 
            curmotif.append(base_dict[base])
            known.append(base_dict[base])
            curscores.append(score) 
        else: 
            #some bases got skipped! 
            delta=pos-curpos-1 
            for i in range(delta): 
                curmotif.append('N') 
                curscores.append('0') 
            curpos=pos 
            curmotif.append(base_dict[base]) 
            known.append(base_dict[base]) 
            curscores.append(score) 
    #store the final motif that hasn't been closed yet! 
    if len(curmotif)>4:
        if len(known)>4: 
            mean_score=sum([float(i) for i in curscores])/len(curscores)            
            outstring=chrom+'\t'+str(firstpos)+'\t'+str(curpos)+'\t'+str(round(mean_score,3))+'\t'+''.join(curmotif)+'\n'
            outf.write(outstring)
    outf.close() 
    #intersect w/ hg19! 
    import subprocess 
    motif_file=outputfile+'.motif' 
    command="bedtools sort -i "+motif_file
    print str(command.split(' '))
    with open(motif_file+".sorted",'w') as out_sorted: 
        subprocess.call(command.split(' '),stdout=out_sorted)    
    print "DONE SORTING!"
    command='bedtools closest -D a -a '+motif_file+'.sorted -b hg19.gff.filtered'
    print str(command) 
    with open(motif_file+".genes","w") as out_genes: 
        subprocess.call(command.split(' '),stdout=out_genes) 

if __name__=="__main__": 
    main() 
