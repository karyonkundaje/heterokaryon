import sys 
mypath=sys.argv[1] 
from os import listdir
from os.path import isfile, join
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
for f in onlyfiles: 
    if f.endswith('.motif_hits')==False: 
        continue 
    #format the file as a bed file 
    outf=open(mypath+'/'+f+'.bed','w')
    outf2=open(mypath+'/'+f+'.hammock','w') 
    data=open(mypath+'/'+f,'r').read().strip().split('\n') 
    idval=0 
    for i in range(0,len(data),2): 
        pos_data=data[i].split('\t')
        try:
            hits=data[i+1].split('\t') 
        except: 
            continue 
        chrom=pos_data[0] 
        start_pos=pos_data[1] 
        end_pos=pos_data[2] 
        if int(end_pos)<=int(start_pos): 
            continue 
        #cannot have duplicates per track :( / need to hack the positions 
        numhits=len(hits)
        start_pos=int(start_pos) 
        end_pos=int(end_pos) 
        width=(end_pos-start_pos)/numhits 
        cur_start=start_pos 
        cur_end=cur_start+width 
        for hit in hits: 
            if hit.startswith('('):                 
                hit_parts=hit.split(' ') 
                cor=str(round(float(hit_parts[0].strip(',').strip('(').strip(')')),3)) 
                match=hit_parts[-1].strip(',').strip('(').strip(')') 
                outf.write(chrom+'\t'+str(cur_start)+'\t'+str(cur_end)+'\t'+match+'\t'+cor+'\n')
                outf2.write(chrom+'\t'+str(cur_start)+'\t'+str(cur_end)+'\t'+'id:'+str(idval)+',name:'+match+',scorelst:['+str(cor)+']'+'\n')
                idval+=1 
                cur_start=cur_end+1 
                cur_end=cur_start+width 
