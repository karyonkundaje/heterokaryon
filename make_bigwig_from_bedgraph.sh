#for i in  X Y Un_gl000246 Un_gl000238 Un_gl000211 17_gl000203_random 19_gl000208_random Un_gl000239 17_gl000206_random Un_gl000249 Un_gl000235 Un_gl000212 Un_gl000222 8_gl000197_random 21_gl000210_random Un_gl000248 Un_gl000237 8_gl000196_random Un_gl000228 Un_gl000227 Un_gl000224 Un_gl000229 Un_gl000232 9_gl000198_random 11_gl000202_random Un_gl000219 Un_gl000242 Un_gl000231 Un_gl000223 4_gl000193_random Un_gl000241 17_gl000204_random Un_gl000234 9_gl000199_random 9_gl000201_random 1_gl000191_random Un_gl000216 1_gl000192_random 17_gl000205_random 7_gl000195_random Un_gl000225 Un_gl000220
#do 
#for j in `seq 0 42`
#do
#    sort -u -k1,1 -k2,2n remapped_allpeaks_chr$i/het_$j\_deepLIFT_0.bedGraph > remapped_allpeaks_chr$i/het_$j\_deepLIFT_0.bedGraph.sorted
#    bedGraphToBigWig remapped_allpeaks_chr$i/het_$j\_deepLIFT_0.bedGraph.sorted /mnt/data/annotations/by_release/hg19.GRCh37/hg19.chrom.sizes remapped_allpeaks_chr$i/het_$j\_deepLIFT_0.bigWig 
#    echo "------" 
#    echo $i
#    echo $j
#    echo "-----" 
#done
#done

for i in `seq 0 11`
do
    #sort -u -k1,1 -k2,2n remapped_allpeaks_chr$i/het_$j\_deepLIFT_0.bedGraph > remapped_allpeaks_chr$i/het_$j\_deepLIFT_0.bedGraph.sorted
    #bedGraphToBigWig oct4/het_$j\_deepLIFT_0.bedGraph /mnt/data/annotations/by_release/hg19.GRCh37/hg19.chrom.sizes oct4/het_$j\_deepLIFT_0.bigWig 
    #bedGraphToBigWig figures_UPDATED/het_$j\_deepLIFT_0.bedGraph.averaged.sorted /mnt/data/annotations/by_release/hg19.GRCh37/hg19.chrom.sizes figures_UPDATED/het_$j\_deepLIFT_0.bigWig 
    sort -u -k1,1 -k2,2n het_$i\_deepLIFT_0.bedGraph.averaged > het_$i\_deepLIFT_0.bedGraph.sorted
    echo $i.sorted
    bedGraphToBigWig het_$i\_deepLIFT\_0.bedGraph.sorted /mnt/data/annotations/by_release/hg19.GRCh37/hg19.chrom.sizes het_$i\_deepLIFT\_0.bedGraph.bigWig
    echo $i.bigWig

done



#for j in `seq 0 42`
#do
#    bedGraphToBigWig lin28a/het_$j\_deepLIFT_0.bedGraph /mnt/data/annotations/by_release/hg19.GRCh37/hg19.chrom.sizes lin28a/het_$j\_deepLIFT_0.bigWig 
#done
