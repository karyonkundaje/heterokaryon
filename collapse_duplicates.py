import sys 
import re 
from collections import OrderedDict 
infile=open(sys.argv[1],'r').read().strip().split('\n') 
outfile=open(sys.argv[2],'w') 
hit_dict=OrderedDict() 
chrom=infile[0].split('\t')[0] 
for line in infile: 
    tokens=line.split('\t') 
    start=tokens[1] 
    end=tokens[2] 
    key=tuple([start,end]) 
    if key not in hit_dict: 
        hit_dict[key]=[tokens[3]]
    else: 
        hit_dict[key].append(tokens[3])
print "indexed filed"
prog=re.compile('\-?\d*\.?\d+[e]?\-?\d*,\d') 
splitter=re.compile(',')  
for entry in hit_dict: 
    if len(hit_dict[entry])==1: 
        outfile.write(chrom+'\t'+entry[0]+'\t'+entry[1]+'\t'+hit_dict[entry][0]+'\n')
    else: 
        #take the highest absolute value! 
        unique_id=':'.join(hit_dict[entry][0].split(':')[0:2]) 
        category_dict=dict() 
        for val in hit_dict[entry]: 
            score_tuples=[re.split(splitter,i) for i in re.findall(prog,val)]
            score_tuples=[[float(i[0]),int(i[1])] for i in score_tuples]
            for t in score_tuples:
                dl=t[0] 
                cat=t[1] 
                if cat not in category_dict: 
                    category_dict[cat]=dl 
                elif abs(dl) > abs(category_dict[cat]): 
                    category_dict[cat]=dl 
        outfile.write(chrom+'\t'+entry[0]+'\t'+entry[1]+'\t'+unique_id+':[')
        val_summary=['['+str(category_dict[i])+','+str(i)+']' for i in category_dict]
        outfile.write(','.join(val_summary)+']\n')
    
        

        
