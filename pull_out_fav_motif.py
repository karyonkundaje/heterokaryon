#GETS ALL INSTANCES OF MOTIF HITS & SORTS BY CORRELATION 
import pdb 
target="nkx3.1"
numtasks=43 
outf=open(target+'.ALL.HITS.tsv','w') 
outf2=open(target+'.TOP.100.tsv','w') 
cor_dict=dict() 
top_hits=dict() 
name_dict=dict() 
name_dict[0]='CC Peak Presenece'
name_dict[1]='3hr Peak Presence' 
name_dict[2]='16hr Peak Presence' 
name_dict[3]='48hr Peak Presence' 
name_dict[4]='H1 Peak Presence' 
name_dict[5]='Hk Peak Presence' 
name_dict[6]='M5 Peak presence' 
name_dict[7]='3hr Upregulated from CC' 
name_dict[8]='3hr Downregulated from CC' 
name_dict[9]='16hr Upregulated from 3hr' 
name_dict[10]='16hr Downregulated from 3hr' 
name_dict[11]='16hr Upregulated from CC' 
name_dict[12]='16hr Downregulated from CC'
name_dict[13]='48hr Upregulated from 16hr' 
name_dict[14]='48hr Downregulated from 16hr'
name_dict[15]='48hr Upregulated from 3hr' 
name_dict[16]='48hr Downregulated from 3hr'
name_dict[17]='48hr Upregulated from CC' 
name_dict[18]='48hr Downregulated from CC'
name_dict[19]='H1 Upregulated from 48hr' 
name_dict[20]='H1 Downregulated from 48hr'
name_dict[21]='H1 Upregulated from 16hr' 
name_dict[22]='H1 Downregulated from 16hr'
name_dict[23]='H1 Upregulated from 3hr' 
name_dict[24]='H1 Downregulated from 3hr'
name_dict[25]='H1 Upregulated from CC' 
name_dict[26]='H1 Downregulated from CC'
name_dict[27]='H1 Upregulated from Hk' 
name_dict[28]='H1 Downregulated from Hk' 
name_dict[29]='48hr Upregulated from Hk' 
name_dict[30]='48hr Downregulated from Hk' 
name_dict[31]='16hr Upregulated from Hk' 
name_dict[32]='16hr Downregulated from Hk' 
name_dict[33]='3hr Upregulated from Hk' 
name_dict[34]='3hr Downregulated from Hk'
name_dict[35]='H1 Upregulated from M5' 
name_dict[36]='H1 Downregulated from M5' 
name_dict[37]='48hr Upregulated from M5' 
name_dict[38]='48hr Downregulated from M5' 
name_dict[39]='16hr Upregulated from M5' 
name_dict[40]='16hr Downregulated from M5' 
name_dict[41]='3hr Upregulated from M5' 
name_dict[42]='3hr Downregulated from M5' 

outf.write('Chrom\tStart\tEnd\t'+'\t'.join([name_dict[i] for i in range(numtasks)])+'\n')
outf2.write('Chrom\tStart\tEnd\tTask\tCorrelation\n') 
for task in range(numtasks): 
    source=open('summary.motif.'+str(task)+'.bed.sorted','r').read().strip().split('\n')
    for line in source: 
        tokens=line.split('\t') 
        if tokens[3].lower().__contains__(target): 
            chrom=tokens[0] 
            startpos=tokens[1] 
            endpos=tokens[2] 
            corval=tokens[4] 
            corval=round(float(corval),3)
            corval=str(corval) 

            if corval not in top_hits: 
                top_hits[corval]=[chrom+'\t'+str(startpos)+'\t'+str(endpos)+'\t'+name_dict[task]+'\t'+str(corval)+'\n']
            else: 
                top_hits[corval].append(chrom+'\t'+str(startpos)+'\t'+str(endpos)+'\t'+name_dict[task]+'\t'+str(corval)+'\n')
            if chrom not in cor_dict: 
                cor_dict[chrom]=dict() 
            if tuple([startpos,endpos]) not in cor_dict[chrom]: 
                cor_dict[chrom][tuple([startpos,endpos])]=dict() 
            cor_dict[chrom][tuple([startpos,endpos])][task]=float(corval) 
#pdb.set_trace() 
#write to the output files!! 
for chrom in cor_dict: 
    for entry in cor_dict[chrom]: 
        outf.write(chrom+'\t'+entry[0]+'\t'+entry[1])
        for task in range(numtasks): 
            if task in cor_dict[chrom][entry]: 
                outf.write('\t'+str(round(float(cor_dict[chrom][entry][task]),3)))
            else: 
                outf.write('\t')
        outf.write('\n') 

#get the top 100! 
#pdb.set_trace() 
keys=top_hits.keys()
keys.sort() 
keys=keys[::-1] 
for key in keys[0:100]: 
    outf2.write(''.join(top_hits[key]))

