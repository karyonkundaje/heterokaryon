#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu7,floatX=float32,lib.cnmem=6500,openmp=True" python make_tracks.py /srv/scratch/annashch/deeplearning/heterokaryon/modelsDir_debug/record_49_model_jcQXD_modelYaml.yaml /srv/scratch/annashch/deeplearning/heterokaryon/modelsDir_debug/record_49_model_jcQXD_modelWeights.h5 /srv/scratch/annashch/deeplearning/heterokaryon/yaml_debug/ small.gdl --chromputer_mark het --output_dir make_tracks_output --save_scores make_tracks_scores --verbose 

#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu7,floatX=float32,lib.cnmem=6500,openmp=True" python make_tracks.py /srv/scratch/annashch/deeplearning/heterokaryon/modelsDir_runsDbPeytonModel_AllPairs_2/record_59_model_8d7fv_modelYaml.yaml /srv/scratch/annashch/deeplearning/heterokaryon/modelsDir_runsDbPeytonModel_AllPairs_2/record_59_model_8d7fv_modelWeights.h5  /srv/scratch/annashch/deeplearning/heterokaryon/yaml_idr_sliced_win50/ test.gdl --chromputer_mark het --output_dir make_tracks_heterokaryon --save_scores make_tracks_scores --verbose 


#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu7,floatX=float32,lib.cnmem=6500,openmp=True" python make_tracks.py /srv/scratch/annashch/deeplearning/heterokaryon/modelsDir_runsDbPeytonModel_AllPairs_2/record_59_model_8d7fv_modelYaml.yaml /srv/scratch/annashch/deeplearning/heterokaryon/modelsDir_runsDbPeytonModel_AllPairs_2/record_59_model_8d7fv_modelWeights.h5  /srv/scratch/annashch/deeplearning/utils/yaml_test /srv/scratch/annashch/deeplearning/utils/test.gdl --chromputer_mark het --output_dir test  --verbose 

#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu7,floatX=float32,lib.cnmem=6500,openmp=True" python make_tracks.py /srv/scratch/annashch/deeplearning/heterokaryon/modelsDir_runsDbPeytonModel_AllPairs_2/record_59_model_8d7fv_modelYaml.yaml /srv/scratch/annashch/deeplearning/heterokaryon/modelsDir_runsDbPeytonModel_AllPairs_2/record_59_model_8d7fv_modelWeights.h5  /srv/scratch/annashch/deeplearning/utils/yaml_1 /srv/scratch/annashch/deeplearning/utils/chr1.gdl --chromputer_mark het --output_dir chr1  --verbose 

#for i in `seq 1 8`
#do
#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu7,floatX=float32,lib.cnmem=6500,openmp=True" python make_tracks.py /srv/scratch/annashch/deeplearning/heterokaryon/modelsDir_runsDbPeytonModel_AllPairs_2/record_59_model_8d7fv_modelYaml.yaml /srv/scratch/annashch/deeplearning/heterokaryon/modelsDir_runsDbPeytonModel_AllPairs_2/record_59_model_8d7fv_modelWeights.h5  /srv/scratch/annashch/deeplearning/utils/yaml_$i /srv/scratch/annashch/deeplearning/utils/chr$i.gdl --chromputer_mark het --output_dir chr$i  --verbose 
#done 

#for i in  X Y 11_gl000202_random 1_gl000191_random 4_gl000194_random Un_gl000219 Un_gl000224 17_gl000205_random 1_gl000192_random 7_gl000195_random Un_gl000220 Un_gl000226
#for i in 10
#do
#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu7,floatX=float32,lib.cnmem=6500,openmp=True" python make_tracks_with_generator.py /srv/scratch/annashch/deeplearning/heterokaryon/modelsDir_runsDbPeytonModel_AllPairs_2/record_82_model_mQtha_modelYaml.yaml /srv/scratch/annashch/deeplearning/heterokaryon/modelsDir_runsDbPeytonModel_AllPairs_2/record_82_model_mQtha_modelWeights.h5  /srv/scratch/annashch/deeplearning/utils/chr$i.hdf5 /srv/scratch/annashch/deeplearning/utils/chr$i.gdl --chromputer_mark het --output_dir chr$i  --verbose 
#done 

#for i in 11_gl000202_random 1_gl000191_random 4_gl000194_random Un_gl000219 Un_gl000224 17_gl000205_random 1_gl000192_random 7_gl000195_random Un_gl000220 Un_gl000226
#do
#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu1,floatX=float32,lib.cnmem=6500,openmp=True" python make_tracks.py /srv/scratch/annashch/deeplearning/heterokaryon/modelsDir_runsDbPeytonModel_AllPairs_2/record_59_model_8d7fv_modelYaml.yaml /srv/scratch/annashch/deeplearning/heterokaryon/modelsDir_runsDbPeytonModel_AllPairs_2/record_59_model_8d7fv_modelWeights.h5  /srv/scratch/annashch/deeplearning/utils/yaml_$i /srv/scratch/annashch/deeplearning/utils/chr$i.gdl --chromputer_mark het --output_dir chr$i  --verbose 
#done 

#for i in `seq 9 10`
#do
#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu0,floatX=float32,lib.cnmem=6500,openmp=True" python make_tracks_with_generator.py /srv/scratch/annashch/deeplearning/dmso/modelsDir_dmso/record_0_model_JRZJS_modelYaml.yaml /srv/scratch/annashch/deeplearning/dmso/modelsDir_dmso/record_0_model_JRZJS_modelWeights.h5  /srv/scratch/annashch/deeplearning/utils/chr$i.hdf5 /srv/scratch/annashch/deeplearning/utils/chr$i.gdl --chromputer_mark het --output_dir dmso_chr$i  --verbose 
#done 

#for i in Un_gl000246 Un_gl000238 Un_gl000211 17_gl000203_random 19_gl000208_random Un_gl000239 17_gl000206_random Un_gl000249 Un_gl000235 Un_gl000212 Un_gl000222 8_gl000197_random 21_gl000210_random Un_gl000248 Un_gl000237 8_gl000196_random Un_gl000228 Un_gl000227 Un_gl000224 Un_gl000229 Un_gl000232 9_gl000198_random 11_gl000202_random Un_gl000219 Un_gl000242 Un_gl000231 Un_gl000223 4_gl000193_random Un_gl000241 17_gl000204_random Un_gl000234 9_gl000199_random 9_gl000201_random 1_gl000191_random Un_gl000216 1_gl000192_random 17_gl000205_random 7_gl000195_random Un_gl000225 Un_gl000220
#for i in 5 9 
#do
#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu7,floatX=float32,lib.cnmem=6500,openmp=True" python make_tracks_with_generator_new_deeplift.py /srv/scratch/annashch/deeplearning/heterokaryon/modelsDir_runsDbPeytonModel_AllPairs_2/record_94_model_AQ8OM_modelYaml.yaml  /srv/scratch/annashch/deeplearning/heterokaryon/modelsDir_runsDbPeytonModel_AllPairs_2/record_94_model_AQ8OM_modelWeights.h5 /srv/scratch/annashch/deeplearning/utils/chr$i.hdf5 /srv/scratch/annashch/deeplearning/utils/chr$i.gdl --chromputer_mark het --output_dir specialized_chr$i  --verbose 
#done 


#for i in `seq 1 22`  X Y Un_gl000246 Un_gl000238 Un_gl000211 17_gl000203_random 19_gl000208_random Un_gl000239 17_gl000206_random Un_gl000249 Un_gl000235 Un_gl000212 Un_gl000222 8_gl000197_random 21_gl000210_random Un_gl000248 Un_gl000237 8_gl000196_random Un_gl000228 Un_gl000227 Un_gl000224 Un_gl000229 Un_gl000232 9_gl000198_random 11_gl000202_random Un_gl000219 Un_gl000242 Un_gl000231 Un_gl000223 4_gl000193_random Un_gl000241 17_gl000204_random Un_gl000234 9_gl000199_random 9_gl000201_random 1_gl000191_random Un_gl000216 1_gl000192_random 17_gl000205_random 7_gl000195_random Un_gl000225 Un_gl000220
#do
#DMSO PEAKS 
#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu3,floatX=float32,lib.cnmem=6500,openmp=True" python make_tracks_with_generator_new_deeplift.py /srv/scratch/annashch/deeplearning/dmso/modelsDir_dmso/bestModel_record_3_model_lQgMF_modelYaml.yaml /srv/scratch/annashch/deeplearning/dmso/modelsDir_dmso/bestModel_record_3_model_lQgMF_modelWeights.h5 /srv/scratch/annashch/deeplearning/dmso/track_input/best/chr$i.hdf5 /srv/scratch/annashch/deeplearning/dmso/track_input/best/chr$i.gdl --chromputer_mark het --output_dir old_dmso_chr$i  --verbose 


#done
#ALL HET PEAKS, REMAPPED 
#for i in `seq 4 5` #X Y Un_gl000246 Un_gl000238 Un_gl000211 17_gl000203_random 19_gl000208_random Un_gl000239 17_gl000206_random Un_gl000249 Un_gl000235 Un_gl000212 Un_gl000222 8_gl000197_random 21_gl000210_random Un_gl000248 Un_gl000237 8_gl000196_random Un_gl000228 Un_gl000227 Un_gl000224 Un_gl000229 Un_gl000232 9_gl000198_random 11_gl000202_random Un_gl000219 Un_gl000242 Un_gl000231 Un_gl000223 4_gl000193_random Un_gl000241 17_gl000204_random Un_gl000234 9_gl000199_random 9_gl000201_random 1_gl000191_random Un_gl000216 1_gl000192_random 17_gl000205_random 7_gl000195_random Un_gl000225 Un_gl000220
#do
#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu7,floatX=float32,lib.cnmem=6500,openmp=True" OMP_NUM_THREADS=12 python make_tracks_with_generator.py /srv/scratch/annashch/deeplearning/heterokaryon/models/modelsDir_runsDbPeytonModel_AllPairs_2/record_92_model_qEE1z_modelYaml.yaml  /srv/scratch/annashch/deeplearning/heterokaryon/models/modelsDir_runsDbPeytonModel_AllPairs_2/record_92_model_qEE1z_modelWeights.h5 /srv/scratch/annashch/deeplearning/heterokaryon/track_input/remapped/chr$i.hdf5 /srv/scratch/annashch/deeplearning/heterokaryon/track_input/remapped/chr$i.gdl --chromputer_mark het --output_dir remapped_allpeaks_chr$i  --verbose 
#done

#SPECIALIZED HET PEAKS
#for i in `seq 1 11`#  X Y Un_gl000246 Un_gl000238 Un_gl000211 17_gl000203_random 19_gl000208_random Un_gl000239 17_gl000206_random Un_gl000249 Un_gl000235 Un_gl000212 Un_gl000222 8_gl000197_random 21_gl000210_random Un_gl000248 Un_gl000237 8_gl000196_random Un_gl000228 Un_gl000227 Un_gl000224 Un_gl000229 Un_gl000232 9_gl000198_random 11_gl000202_random Un_gl000219 Un_gl000242 Un_gl000231 Un_gl000223 4_gl000193_random Un_gl000241 17_gl000204_random Un_gl000234 9_gl000199_random 9_gl000201_random 1_gl000191_random Un_gl000216 1_gl000192_random 17_gl000205_random 7_gl000195_random Un_gl000225 Un_gl000220
#do
#    THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu7,floatX=float32,lib.cnmem=6500,openmp=True" python make_tracks_with_generator_new_deeplift.py /srv/scratch/annashch/deeplearning/heterokaryon/modelsDir_runsDbPeytonModel_AllPairs_2/record_94_model_AQ8OM_modelYaml.yaml  /srv/scratch/annashch/deeplearning/heterokaryon/modelsDir_runsDbPeytonModel_AllPairs_2/record_94_model_AQ8OM_modelWeights.h5 /srv/scratch/annashch/deeplearning/heterokaryon/track_input/specialized/chr$i.hdf5 /srv/scratch/annashch/deeplearning/heterokaryon/track_input/specialized/chr$i.gdl --chromputer_mark het --output_dir old_deepLIFT_specialized_chr$i  --verbose 
#done 


#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu7,floatX=float32,lib.cnmem=6500,openmp=True" python make_tracks_with_generator.py /srv/scratch/annashch/deeplearning/dmso/modelsDir_dmso/bestModel_record_1_model_sDmFf_modelYaml.yaml /srv/scratch/annashch/deeplearning/dmso/modelsDir_dmso/bestModel_record_1_model_sDmFf_modelWeights.h5 /srv/scratch/annashch/deeplearning/dmso/inputs/regression.hdf5 /srv/scratch/annashch/dmso/all.gdl --chromputer_mark het --output_dir regression_dmso  --verbose 

#FIGURES FOR 6 GENES OF INTEREST 
#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu3,floatX=float32,lib.cnmem=6500,openmp=True" OMP_NUM_THREADS=12 python make_tracks_with_generator.py /srv/scratch/annashch/deeplearning/heterokaryon/models/modelsDir_runsDbPeytonModel_AllPairs_2/record_92_model_qEE1z_modelYaml.yaml  /srv/scratch/annashch/deeplearning/heterokaryon/models/modelsDir_runsDbPeytonModel_AllPairs_2/record_92_model_qEE1z_modelWeights.h5 /srv/scratch/annashch/deeplearning/heterokaryon/track_input/figures/figure.hdf5 /srv/scratch/annashch/deeplearning/heterokaryon/track_input/figures/figure.gdl --chromputer_mark het --output_dir figure --verbose

#OCT4 corrected positions 
#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu3,floatX=float32,lib.cnmem=6500,openmp=True" OMP_NUM_THREADS=12 python make_tracks_with_generator.py /srv/scratch/annashch/deeplearning/heterokaryon/runsDb/modelsDir_runsDb/record_133_model_CNicM_modelYaml.yaml /srv/scratch/annashch/deeplearning/heterokaryon/runsDb/modelsDir_runsDb/record_133_model_CNicM_modelWeights.h5  /srv/scratch/annashch/deeplearning/heterokaryon/track_input/oct4/oct4.hdf5 /srv/scratch/annashch/deeplearning/heterokaryon/track_input/oct4/oct4.gdl --chromputer_mark het --output_dir oct4  --verbose 

#LIN28A
#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu7,floatX=float32,lib.cnmem=6500,openmp=True" OMP_NUM_THREADS=12 python make_tracks_with_generator.py /srv/scratch/annashch/deeplearning/heterokaryon/runsDb/modelsDir_runsDb/record_133_model_CNicM_modelYaml.yaml /srv/scratch/annashch/deeplearning/heterokaryon/runsDb/modelsDir_runsDb/record_133_model_CNicM_modelWeights.h5  /srv/scratch/annashch/deeplearning/heterokaryon/track_input/lin28a/lin28a.hdf5 /srv/scratch/annashch/deeplearning/heterokaryon/track_input/lin28a/lin28a.gdl --chromputer_mark het --output_dir lin28a  --verbose 


#2kb regions around each of the het peaks 
#for i in  `seq 19 22` X Y Un_gl000246 Un_gl000238 Un_gl000211 17_gl000203_random 19_gl000208_random Un_gl000239 17_gl000206_random Un_gl000249 Un_gl000235 Un_gl000212 Un_gl000222 8_gl000197_random 21_gl000210_random Un_gl000248 Un_gl000237 8_gl000196_random Un_gl000228 Un_gl000227 Un_gl000224 Un_gl000229 Un_gl000232 9_gl000198_random 11_gl000202_random Un_gl000219 Un_gl000242 Un_gl000231 Un_gl000223 4_gl000193_random Un_gl000241 17_gl000204_random Un_gl000234 9_gl000199_random 9_gl000201_random 1_gl000191_random Un_gl000216 1_gl000192_random 17_gl000205_random 7_gl000195_random Un_gl000225 Un_gl000220
#do
#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu6,floatX=float32,lib.cnmem=6500,openmp=True" OMP_NUM_THREADS=12 python make_tracks_with_generator.py /srv/scratch/annashch/deeplearning/heterokaryon/runsDb/modelsDir_runsDb/record_133_model_CNicM_modelYaml.yaml /srv/scratch/annashch/deeplearning/heterokaryon/runsDb/modelsDir_runsDb/record_133_model_CNicM_modelWeights.h5  /srv/scratch/annashch/deeplearning/heterokaryon/track_input/debug.2kb.no/chr$i.hdf5 /srv/scratch/annashch/deeplearning/heterokaryon/track_input/debug.2kb.no/chr$i.gdl --chromputer_mark het --output_dir debug.2kb.no_chr$i  --verbose 
#done

#all input het peaks -- these are overlapping! Must average the deeplift scores 
#for i in `seq 1 22`  X Y Un_gl000246 Un_gl000238 Un_gl000211 17_gl000203_random 19_gl000208_random Un_gl000239 17_gl000206_random Un_gl000249 Un_gl000235 Un_gl000212 Un_gl000222 8_gl000197_random 21_gl000210_random Un_gl000248 Un_gl000237 8_gl000196_random Un_gl000228 Un_gl000227 Un_gl000224 Un_gl000229 Un_gl000232 9_gl000198_random 11_gl000202_random Un_gl000219 Un_gl000242 Un_gl000231 Un_gl000223 4_gl000193_random Un_gl000241 17_gl000204_random Un_gl000234 9_gl000199_random 9_gl000201_random 1_gl000191_random Un_gl000216 1_gl000192_random 17_gl000205_random 7_gl000195_random Un_gl000225 Un_gl000220
#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu7,floatX=float32,lib.cnmem=6500,openmp=True" OMP_NUM_THREADS=12 python make_tracks_with_generator.py /srv/scratch/annashch/deeplearning/heterokaryon/runsDb/modelsDir_runsDb/record_133_model_CNicM_modelYaml.yaml /srv/scratch/annashch/deeplearning/heterokaryon/runsDb/modelsDir_runsDb/record_133_model_CNicM_modelWeights.h5  /srv/scratch/annashch/deeplearning/heterokaryon/track_input/debug/chr$i.hdf5 /srv/scratch/annashch/deeplearning/heterokaryon/track_input/debug/chr$i.gdl --chromputer_mark het --output_dir debug  --verbose 


#ALL THE GENES OF INTEREST!
#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu7,floatX=float32,lib.cnmem=6500,openmp=True" OMP_NUM_THREADS=12 python make_tracks_with_generator_new_deeplift.py /srv/scratch/annashch/deeplearning/heterokaryon/runsDb/modelsDir_runsDb/record_133_model_CNicM_modelYaml.yaml /srv/scratch/annashch/deeplearning/heterokaryon/runsDb/modelsDir_runsDb/record_133_model_CNicM_modelWeights.h5  /srv/scratch/annashch/deeplearning/heterokaryon/track_input/figures.2kb.no/figures.hdf5 /srv/scratch/annashch/deeplearning/heterokaryon/track_input/figures.2kb.no/figures.gdl --chromputer_mark het --output_dir figures_FINAL_2KB_NO  --verbose 

#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu3,floatX=float32,lib.cnmem=6500,openmp=True" OMP_NUM_THREADS=12 python make_tracks_with_generator_new_deeplift.py /srv/scratch/annashch/deeplearning/heterokaryon/runsDb/modelsDir_runsDb/record_133_model_CNicM_modelYaml.yaml /srv/scratch/annashch/deeplearning/heterokaryon/runsDb/modelsDir_runsDb/record_133_model_CNicM_modelWeights.h5  /srv/scratch/annashch/deeplearning/heterokaryon/track_input/figures/figures.hdf5 /srv/scratch/annashch/deeplearning/heterokaryon/track_input/figures/figures.gdl --chromputer_mark het --output_dir figures_FINAL  --verbose 



#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu7,floatX=float32,lib.cnmem=6500,openmp=True" OMP_NUM_THREADS=12 python make_tracks_with_generator.py /srv/scratch/annashch/deeplearning/heterokaryon/runsDb/modelsDir_runsDb/record_133_model_CNicM_modelYaml.yaml /srv/scratch/annashch/deeplearning/heterokaryon/runsDb/modelsDir_runsDb/record_133_model_CNicM_modelWeights.h5  /srv/scratch/annashch/deeplearning/heterokaryon/track_input/figures.2kb.no/figures.hdf5 /srv/scratch/annashch/deeplearning/heterokaryon/track_input/figures.2kb.no/figures.gdl --chromputer_mark het --output_dir figures_2KB_NO  --verbose 

#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu2,floatX=float32,lib.cnmem=6500,openmp=True" OMP_NUM_THREADS=12 python make_tracks_with_generator.py /srv/scratch/annashch/deeplearning/heterokaryon/runsDb/modelsDir_runsDb/record_133_model_CNicM_modelYaml.yaml /srv/scratch/annashch/deeplearning/heterokaryon/runsDb/modelsDir_runsDb/record_133_model_CNicM_modelWeights.h5  /srv/scratch/annashch/deeplearning/heterokaryon/track_input/figures/figures.hdf5 /srv/scratch/annashch/deeplearning/heterokaryon/track_input/figures/figures.gdl --chromputer_mark het --output_dir figures  --verbose 



#DMSO NEW DATASET! #STARTING AT 400 
#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu3,floatX=float32,lib.cnmem=6500,openmp=True" OMP_NUM_THREADS=12
#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu3,floatX=float32" OMP_NUM_THREADS=12 python make_tracks_with_generator_new_deeplift.py /srv/scratch/annashch/deeplearning/dmso/modelsDir_dmso/bestModel_record_0_model_sPqDv_modelYaml.yaml /srv/scratch/annashch/deeplearning/dmso/modelsDir_dmso/bestModel_record_0_model_sPqDv_modelWeights.h5  /srv/scratch/annashch/deeplearning/dmso/track_input/newdata/newdata.hdf5 /srv/scratch/annashch/deeplearning/dmso/track_input/newdata/dmso.new.gdl --chromputer_mark het --output_dir dmso_NEWDATA  --verbose 

#DMSO batch 2 with updated negative set
#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu3,floatX=float32" OMP_NUM_THREADS=12 python make_tracks_with_generator_new_deeplift.py /srv/scratch/annashch/deeplearning/dmso/modelsDir_dmso/bestModel_record_1_model_vVGDA_modelYaml.yaml /srv/scratch/annashch/deeplearning/dmso/modelsDir_dmso/bestModel_record_1_model_vVGDA_modelWeights.h5  /srv/scratch/annashch/deeplearning/dmso/track_input/newNegativeSet/newNegativeSet.forDL.hdf5 /srv/scratch/annashch/deeplearning/dmso/track_input/newNegativeSet/newNegativeSet.labels.txt.gdl --chromputer_mark het --output_dir dmso_NEWDATA  --verbose 

#DMSO batch 2 with frozen filters 
#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu3,floatX=float16" OMP_NUM_THREADS=12 python make_tracks_with_generator_new_deeplift.py /srv/scratch/annashch/deeplearning/dmso/model_md/record_2_model_kj8CF_modelYaml.yaml /srv/scratch/annashch/deeplearning/dmso/model_md/record_2_model_kj8CF_modelWeights.h5  /srv/scratch/annashch/deeplearning/dmso/inputs/newNegativeSet_ForDeepLIFT/deeplift_data.hdf5 /srv/scratch/annashch/deeplearning/dmso/inputs/newNegativeSet_ForDeepLIFT.gdl --chromputer_mark het --output_dir dmso_FROZEN_FILTERS_FIRST_LAYER --verbose --default_input_mode_name default_input_mode_name --pre_activation_target_layer_name fc_3 --batch_size 50


#heterokaryon with frozen filters 
#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu7,floatX=float16" OMP_NUM_THREADS=12 python make_tracks_with_generator_new_deeplift.py /srv/scratch/annashch/deeplearning/heterokaryon/model_md/record_24_model_Snkxa_modelYaml.yaml /srv/scratch/annashch/deeplearning/heterokaryon/model_md/record_24_model_Snkxa_modelWeights.h5  /srv/scratch/annashch/deeplearning/heterokaryon/inputs/new.deeplift/train_data.hdf5 /srv/scratch/annashch/deeplearning/heterokaryon/inputs/new.deeplift.gdl --chromputer_mark het --output_dir het_FROZEN_FILTERS_FIRST_LAYER --verbose --default_input_mode_name default_input_mode_name --pre_activation_target_layer_name fc_3 --batch_size 50


#THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu1,floatX=float16" OMP_NUM_THREADS=12 python make_tracks_with_generator_new_deeplift.py /srv/scratch/annashch/deeplearning/heterokaryon/model_md/record_24_model_Snkxa_modelYaml.yaml /srv/scratch/annashch/deeplearning/heterokaryon/model_md/record_24_model_Snkxa_modelWeights.h5  /srv/scratch/annashch/deeplearning/heterokaryon/inputs/new.deeplift.figures/train_data.hdf5 /srv/scratch/annashch/deeplearning/heterokaryon/inputs/new.deeplift.figures.gdl --chromputer_mark het --output_dir het_FROZEN_FILTERS_FIRST_LAYER_FIGURES2 --verbose --default_input_mode_name default_input_mode_name --pre_activation_target_layer_name fc_3 --batch_size 200


#filter pre-initialization 
THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu1,floatX=float16" OMP_NUM_THREADS=12 python make_tracks_with_generator_new_deeplift.py /srv/scratch/annashch/deeplearning/heterokaryon/model_md/record_1_model_yxMUF_modelYaml.yaml /srv/scratch/annashch/deeplearning/heterokaryon/model_md/record_1_model_yxMUF_modelWeights.h5  /srv/scratch/annashch/deeplearning/heterokaryon/inputs/s_by_m/s_by_3m_test.hdf5 /srv/scratch/annashch/deeplearning/heterokaryon/inputs/new.deeplift.figures.gdl --chromputer_mark het --output_dir het_S_BY_3M --verbose --default_input_mode_name sequence --pre_activation_target_layer_name fc_2 --batch_size 200

