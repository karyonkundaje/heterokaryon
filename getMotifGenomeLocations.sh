#ALL HET PEAKS, REMAPPED 
for i in X Y Un_gl000246 Un_gl000238 Un_gl000211 17_gl000203_random 19_gl000208_random Un_gl000239 17_gl000206_random Un_gl000249 Un_gl000235 Un_gl000212 Un_gl000222 8_gl000197_random 21_gl000210_random Un_gl000248 Un_gl000237 8_gl000196_random Un_gl000228 Un_gl000227 Un_gl000224 Un_gl000229 Un_gl000232 9_gl000198_random 11_gl000202_random Un_gl000219 Un_gl000242 Un_gl000231 Un_gl000223 4_gl000193_random Un_gl000241 17_gl000204_random Un_gl000234 9_gl000199_random 9_gl000201_random 1_gl000191_random Un_gl000216 1_gl000192_random 17_gl000205_random 7_gl000195_random Un_gl000225 Un_gl000220
do
    THEANO_FLAGS="mode=FAST_RUN,nvcc.fastmath=True,device=gpu6,floatX=float32,lib.cnmem=6500,openmp=True" OMP_NUM_THREADS=12 python getMotifGenomeLocations.py /srv/scratch/annashch/deeplearning/heterokaryon/models/modelsDir_runsDbPeytonModel_AllPairs_2/record_92_model_qEE1z_modelYaml.yaml  /srv/scratch/annashch/deeplearning/heterokaryon/models/modelsDir_runsDbPeytonModel_AllPairs_2/record_92_model_qEE1z_modelWeights.h5 /srv/scratch/annashch/deeplearning/heterokaryon/track_input/remapped/chr$i.hdf5 /srv/scratch/annashch/deeplearning/heterokaryon/track_input/remapped/chr$i.gdl --chromputer_mark het --output_dir motifGenomeLocations_chr$i  --verbose 
    echo "------------------------------------------------"
    echo $i
    echo "------------------------------------------------"
    #python formatMotifHitsAsTracks.py motifGenomeLocations_chr$i
    #cd motifGenomeLocations_chr$i
    #for j in *hammock.sorted 
    #do
	#bedtools sort -i $j > $j.sorted 
	#bgzip $j 
	#tabix -p bed $j.gz 
    #done
    #cd ..
    #echo "------------------------------------------------"
done
