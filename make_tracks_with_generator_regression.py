import sys
import argparse
from collections import OrderedDict
from itertools import izip
import logging
import os
import subprocess
import numpy as np
from pysam import tabix_index
import h5py

os.environ['GDL_NOCAFFE'] = ''
sys.path.insert(0,"/users/avanti/caffe/python/")
from genomedatalayer.gdlfile import GDLFile
#from skcaffe.util import mkdir_p
import pdb 
#######################################################################
# Setup environment for av_scripts and DeepLIFT imports
import keras
import theano

scriptsDir = os.environ.get("UTIL_SCRIPTS_DIR");
sys.path.insert(0,scriptsDir);
scriptsDir = os.environ.get("ENHANCER_SCRIPTS_DIR");
sys.path.insert(0,scriptsDir+"/featureSelector/deepLIFFT/");
import deepLIFTutils
scriptsDir = os.environ.get("KERAS_DIR");
import glob
import os
import compare_filters_to_known_motifs
from importDataPackage import importData
import pathSetter;
import util;
import fileProcessing as fp
import keras
from plottingUtilitiesPackage import matplotlibHelpers as mplh;
import criticalSubsetIdentification as csi

sys.path.insert(0,scriptsDir)
sys.path.insert(0,scriptsDir+"/featureSelector/deepLIFFT/kerasBasedBackprop");
from deepLIFTonGPU import ScoreTypes, Activations_enum, OutLayerInfo, getScoreFunc
import miscDeepLIFTutils
np.random.seed(1234)
#####################################################
import pyximport
_importers = pyximport.install()
from qcatIO import write_score_for_single_interval
pyximport.uninstall(*_importers)

# Setup logging
log_formatter = \
    logging.Formatter('%(levelname)s:%(asctime)s:%(name)s] %(message)s')

logger = logging.getLogger('make_tracks')
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
handler.setFormatter(log_formatter)
logger.setLevel(logging.INFO)
logger.addHandler(handler)
logger.propagate = False

# Magic numbers and constants here (that are not in args)
BLOB_DIMS = 4
SEQUENCE_CONV_LAYER_NAME = 'sequence-conv1'


def parse_args():
    parser = argparse.ArgumentParser(description='Make DeepLIFT tracks')

    parser.add_argument('model_yaml', help='Keras model yaml')
    parser.add_argument('model_weights', help='Keras model weights (HDF5)')
    parser.add_argument('hdf5', help='hdf5 of the input data')
    parser.add_argument('gdl_file', help='GDL file corresponding to input')

    parser.add_argument('--chromputer_mark',
                        help='Mark name (for chromputer models)',
                        required=True)

    parser.add_argument('--output_dir', help='Output directory for tracks',
                        default='')
    parser.add_argument('--save_scores',
                        help='Compute all DeepLIFT scores and save')
    parser.add_argument('--verbose', help='Verbose mode', action='store_true')

    return parser.parse_args()


def names_from_mark(model, mark):
    pdb.set_trace() 
    softmax_layer, = [layer for layer in model.nodes.keys()
                      if layer.endswith('{}_softmax'.format(mark))]
    ip_layer = softmax_layer.split('_')[0]
    return mark, mark + '_loss', ip_layer


def expand_dims_blob(arr, target_num_axes=BLOB_DIMS):
    # Reshapes arr, adds dims after the first axis
    assert len(arr.shape) <= BLOB_DIMS
    extra_dims = target_num_axes - len(arr.shape)
    new_shape = (arr.shape[0],) + (1,)*extra_dims + tuple(arr.shape[1:])
    return arr.reshape(new_shape)


def _write_2D_deeplift_track(scores, intervals, file_prefix,first,last,line_id, reorder,
                             categories):
    # Writes out track as a quantitative category series:
    # http://wiki.wubrowse.org/QuantitativeCategorySeries
    # TODO: implement reorder = False
    if not reorder:
        raise NotImplementedError

    assert scores.ndim == 3

    logger.info('Writing 2D track of shape: {}'.format(scores.shape))
    logger.info('Writing to file: {}'.format(file_prefix))

    if categories is None:
        categories = np.arange(scores.shape[1])
    #if this is the first time the function is called, open the output file, otherwise append to existing file! 
    if first: 
        with open(file_prefix, 'w') as fp:
            line_id = 0
            for interval, score in izip(intervals, scores):
                line_id = write_score_for_single_interval(fp, interval, score,line_id, categories)
    else: 
        with open(file_prefix, 'a') as fp:
            for interval, score in izip(intervals, scores):
                line_id = write_score_for_single_interval(fp, interval, score,
                                                      line_id, categories)
    logger.info('Wrote hammock file.')
    #collapse duplicates from the hammock file -- keep the entry with the highest abs(deepLIFT) 
    if last: 
        '''
        try:
            collapse_command="python collapse_duplicates.py "+file_prefix +" " + file_prefix+".collapsed" 
            collapse_command=collapse_command.split(' ') 
            print collapse_command 
            subprocess.call(collapse_command) 
        except subprocess.CalledProcessError as e:
            logger.error('collapse_duplicates.py terminated with exit code {}'.format(
                e.returncode))
            logger.error('output was:\n' + e.output)
        '''
        #bedtools sort the hammock file ! 
        try:
            sort_command="bedtools sort -i "+file_prefix#+".collapsed" 
            sort_command=sort_command.split(' ') 
            print sort_command 
            with open(file_prefix+'.sorted','w') as outfile: 
                subprocess.call(sort_command,stdout=outfile)
        except subprocess.CalledProcessError as e:
            logger.error('bedtools sort terminated with exit code {}'.format(
                e.returncode))
            logger.error('output was:\n' + e.output)

        compressed_file = tabix_index(file_prefix+".sorted", preset='bed', force=True)
        assert compressed_file == file_prefix+".sorted" + '.gz'
        logger.info('Compressed and indexed hammock file.')
    return line_id 

CHROM_SIZES = '/mnt/data/annotations/by_release/hg19.GRCh37/hg19.chrom.sizes'


def _write_1D_deeplift_track(scores, intervals, file_prefix):
    assert scores.ndim == 2

    bedgraph = file_prefix + '.bedGraph'
    bigwig = file_prefix + '.bw'

    logger.info('Writing 1D track of shape: {}'.format(scores.shape))
    logger.info('Writing to file: {}'.format(bigwig))

    with open(bedgraph, 'w') as fp:
        for interval, score in izip(intervals, scores):
            chrom = interval.chrom
            start = interval.start
            for score_idx, val in enumerate(score):
                fp.write('%s\t%d\t%d\t%g\n' % (chrom,
                                               start + score_idx,
                                               start + score_idx + 1,
                                               val))
    logger.info('Wrote bedgraph.')

    try:
        output = subprocess.check_output(
            ['wigToBigWig', bedgraph, CHROM_SIZES, bigwig],
            stderr=subprocess.STDOUT)
        logger.info('wigToBigWig output: {}'.format(output))
    except subprocess.CalledProcessError as e:
        logger.error('wigToBigWig terminated with exit code {}'.format(
            e.returncode))
        logger.error('output was:\n' + e.output)

    logger.info('Wrote bigwig.')


def write_deeplift_track(scores, intervals, file_prefix, first,last,line_id,reorder=True,
                         categories=None,):
    if len(scores.shape) != BLOB_DIMS:
        raise ValueError('scores should have same number of dims as a blob')
    if scores.shape[0] != len(intervals):
        raise ValueError('intervals list should have the same number of '
                         'elements as the number of rows in scores')

    # don't squeeze out the first (samples) dimension
    squeezable_dims = tuple(dim for dim, size in enumerate(scores.shape)
                            if size == 1 and dim > 0)
    scores = scores.squeeze(axis=squeezable_dims)
    signal_dims = scores.ndim - 1
    line_id=0 
    if signal_dims == 2:
       line_id= _write_2D_deeplift_track(scores, intervals, file_prefix,first,last,line_id,reorder=True,categories=categories)
    elif signal_dims == 1:
        _write_1D_deeplift_track(scores, intervals, file_prefix)
    else:
        raise ValueError('Cannot handle scores with {} signal dims;'
                         'Only 1D/2D signals supported'.format(signal_dims))
    return line_id 



def partition_intervals(intervals):
    # Partition interval list into several lists of non-overlapping intervals
    # We need this because of subpeaks whose windows will overlap
    # partitions will contain a list of partitions, which is itself a list
    # containing (index, interval) pairs, where the index is such that
    # intervals[index] = interval for the original intervals list passed in.
    from pybedtools import Interval

    def overlap(i1, i2):
        if i1.chrom != i2.chrom:
            return False
        return (i2.start < i1.end and i2.end > i1.start)

    partitions = []
    # sort intervals to get a list of (index, interval) pairs such that
    # intervals[index] = interval
    remaining = sorted(
        enumerate(intervals),
        key=lambda item: (item[1].chrom, item[1].start, item[1].stop))

    while remaining:
        nonoverlapping = [(-1, Interval('sentinel', 0, 0))]
        overlapping = []

        for idx, interval in remaining:
            if not overlap(nonoverlapping[-1][1], interval):
                nonoverlapping.append((idx, interval))
            else:
                overlapping.append((idx, interval))

        partitions.append(nonoverlapping[1:])
        remaining = overlapping

    return partitions

# *** MAIN SCRIPT ***
args = parse_args()
output_name = args.chromputer_mark
output_prefix = os.path.join(args.output_dir, output_name)
try:
    os.mkdir(args.output_dir)
except: 
    print(" output directory exists!") 
# load model
logger.info('Loading model and weights...')
model = deepLIFTutils.loadKerasModel(args.model_weights, args.model_yaml)
deepLIFTutils.meanNormaliseFirstConvLayerWeights(model);
print('got model!')
# load data with hdf5 generator

inputFile=h5py.File(args.hdf5,'r')  
x_key="trainX" 
y_key="trainY"
numEntries=inputFile[x_key].shape[0]  
numtasks=inputFile[y_key].shape[1] 
###################################################################################
# load GDL
gdl_file = GDLFile(args.gdl_file)
logger.info('Loaded GDL file: {} intervals'.format(gdl_file.num_intervals))
logger.info('Computing deepLIFT scores...')
trackLayers = [0] #final maxpooling layer! 
trackLayerName = 0
#scoreTypes=[ScoreTypes.deepLIFT_rawContrib,ScoreTypes.sensitivity]
scoreTypes=[ScoreTypes.deepLIFT]
outputLayers = [OutLayerInfo(outLayNoAct=model.layers[-1],activation='sigmoid')]
import deepLIFTonGPU
scoreFunc = deepLIFTonGPU.getScoreFunc(model, trackLayers, outputLayers, scoreTypes);
#run the scoring function on the full validation set in batches 
batchsize= 10000 
batchindex=0 
first=True 
last=False 
line_id_dict=dict() 
for i in range(numtasks): 
    line_id_dict[i]=0 
while batchindex < numEntries: 
    end_index=min([batchindex+batchsize,numEntries-1])
    if end_index==(numEntries-1): 
        last=True 
    print "batchindex:"+str(batchindex) 
    print "endindex:"+str(end_index) 
    cur_data_X=np.asarray(inputFile[x_key][batchindex:end_index])
    cur_data_Y=np.asarray(inputFile[y_key][batchindex:end_index]) 
    cur_outputs = deepLIFTutils.getSequentialModelLayerOutputs(
                                        model
                                        , inputDatas=[cur_data_X] 
                                        , layerIdx=-1
                                        , batchSize=500 
                                        , progressUpdate=10000)
    cur_outputs=np.array(cur_outputs)
    allDLOutputOnData = deepLIFTutils.runScoreFuncOnData(batchSize=500
                                                          , data=[cur_data_X]
                                                          , scoreFunc=scoreFunc
                                                          , progressUpdate=10000)

    dlValidRawContribs_singleNeuron = OrderedDict([(scoreType, dict()) for scoreType in scoreTypes])
    interval_dict=dict() 
    intervals=gdl_file.intervals[batchindex:end_index] 
    for scoreType in scoreTypes:
        for neuronOfInterest_idx in range(numtasks): 
            #figure out what the true positives are
            labelsForNeuronOfInterest = [x[neuronOfInterest_idx] for x in cur_data_Y]
            outputsForNeuronOfInterest = [x[neuronOfInterest_idx] for x in cur_outputs]
            threshold=0.5
            truePositiveIndices = csi.getTruePositiveIndicesAboveThreshold(
                outputs=outputsForNeuronOfInterest
                , trueLabels=labelsForNeuronOfInterest
                , thresholdProb=threshold
                , classifProb=threshold)

            print(len(truePositiveIndices))
            singleNeuronContribs =allDLOutputOnData[scoreType][0][trackLayerName][neuronOfInterest_idx][truePositiveIndices]
            dlValidRawContribs_singleNeuron[scoreType][neuronOfInterest_idx] = singleNeuronContribs 
            interval_dict[neuronOfInterest_idx]=[intervals[i] for  i in truePositiveIndices]

    deeplift_scores=dlValidRawContribs_singleNeuron 
    logger.info('Writing tracks...')
    for score_type, scoredict in deeplift_scores.iteritems():
        for track_name, scores in scoredict.iteritems():
            partition_idx=0 
            logger.info('Writing track: {} {}'.format(track_name, score_type))
            file_prefix = '{}_{}_{}_{}'.format(output_prefix, track_name,
                                               score_type, partition_idx)
            line_id_dict[track_name]=write_deeplift_track(scores, interval_dict[track_name], file_prefix,first,last,line_id_dict[track_name])
    batchindex+=batchsize 
    first=False 
