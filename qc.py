import deeplift
from deeplift.conversion import keras_conversion as kc
model_weights="/srv/scratch/annashch/deeplearning/dmso/modelsDir_dmso/bestModel_record_0_model_sPqDv_modelWeights.h5"
model_yaml="/srv/scratch/annashch/deeplearning/dmso/modelsDir_dmso/bestModel_record_0_model_sPqDv_modelYaml.yaml"
model=kc.load_keras_model(model_weights,model_yaml,normalise_conv_for_one_hot_encoded_input=True)
