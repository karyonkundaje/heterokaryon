CHROM_SIZES = '/mnt/data/annotations/by_release/hg19.GRCh37/hg19.chrom.sizes'
thresh=1e-3

def _write_1D_deeplift_track(scores, intervals, file_prefix,first):
    assert scores.ndim == 2

    bedgraph = file_prefix + '.bedGraph'
    logger.info('Writing 1D track of shape: {}'.format(scores.shape))
    if first: 
        with open(bedgraph, 'w') as fp:
            for interval, score in izip(intervals, scores):
                chrom = interval.chrom
                start = interval.start
                outputline="" 
                for score_idx, val in enumerate(score):
                    if abs(val) > thresh: 
                        outputline=outputline+chrom+'\t'+str(start+score_idx)+'\t'+str(start+score_idx+1)+'\t'+str(val)+'\n' 
                fp.write(output_line) 
        logger.info('Wrote bedgraph.')
    else: 
        with open(bedgraph, 'a') as fp:
            #output_line="" 
            for interval, score in izip(intervals, scores):
                chrom = interval.chrom
                start = interval.start
                outputline=""
                for score_idx, val in enumerate(score):
                    if abs(val) > thresh: 
                        outputline=outputline+chrom+'\t'+str(start+score_idx)+'\t'+str(start+score_idx+1)+'\t'+str(val)+'\n'
                fp.write(output_line)                 
        logger.info('Wrote bedgraph.')        

