#EXTRACT MOTIFS FROM FILTERED DEEPLIFT FILES 
import sys 
import pdb
data=open(sys.argv[1],'r').read().strip().split('\n') 
outf=open(sys.argv[2],'w') 
chrom=sys.argv[3] 
curpos=None 
curmotif=[] 
curscores=[]
known=[] 
firstpos=None 
base_dict=dict() 
base_dict['0']='A' 
base_dict['1']='C' 
base_dict['2']='G' 
base_dict['3']='T' 

maxgap=5 # maximum gap between bases with significant deepLIFT scores in order for them to be considered part of same motif 
for line in data:
    #pdb.set_trace() 
    tokens=line.split('\t') 
    pos=int(tokens[1])
    baseval=tokens[3].split('[')[2].split(']')[0].replace(' ','').split(',') 
    score=baseval[0] 
    base=baseval[1] 
    if firstpos==None: 
        #first time we see a base! start first motif 
        firstpos=pos 
        curpos=pos 
        curmotif=[base_dict[base]] 
        known=[base_dict[base]]
        curscores=[score]
    elif (pos - curpos) > maxgap:
        #store current motif & reset! 
        if len(curmotif)>4: 
            if len(known)>4: 
                outstring=chrom+'\t'+str(firstpos)+'\t'+str(curpos)+'\t'+'_'.join(curscores)+'\t'+''.join(curmotif)+'\n'
                outf.write(outstring) 
        #reset everything! 
        curpos=pos 
        curmotif=[base_dict[base]] 
        known=[base_dict[base]]
        curscores=[score]
        firstpos=pos 
    elif (pos-curpos==1): 
        #no bases skipped, update current motif 
        curpos=pos 
        curmotif.append(base_dict[base])
        known.append(base_dict[base])
        curscores.append(score) 
    else: 
        #some bases got skipped! 
        delta=pos-curpos-1 
        for i in range(delta): 
            curmotif.append('N') 
            curscores.append('0') 
        curpos=pos 
        curmotif.append(base_dict[base]) 
        known.append(base_dict[base]) 
        curscores.append(score) 
#store the final motif that hasn't been closed yet! 
if len(curmotif)>4:
    if len(known)>4: 
        outstring=chrom+'\t'+str(firstpos)+'\t'+str(curpos)+'\t'+'_'.join(curscores)+'\t'+''.join(curmotif)+'\n'
        outf.write(outstring)
