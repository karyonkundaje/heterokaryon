#Adds motif information from Homer's findMotifs.pl as applied to Homer, Encode, CISBP motifs 
import sys 
gene_file=open(sys.argv[1],'r').read().strip().split('\n') 
motif_file=open(sys.argv[2],'r').read().strip().split('\n')  
outfile=open(sys.argv[1]+'.withMotifs','w') 
outfile.write('CHROM\tSTART_BASE\tEND_BASE\tMEAN_DEEPLIFT\tNEAREST_GENE\tNEAREST_GENE_COMPONENT\tDISTANCE_TO_GENE\tMOTIF_OFFSET_IN_SEQ\tMOTIF_SEQUENCE\tMOTIF_NAME\tMOTIF_STRAND\tMOTIF_SCORE\n')
motif_dict=dict() 
for line in motif_file[1::]:
    tokens=line.split('\t') 
    line_id=int(tokens[0])
    motif_dict[line_id]='\t'.join(tokens[1::]) 
print "made motif dict!!" 
for i in range(len(gene_file)): 
    line=gene_file[i] 
    if i in motif_dict: 
        outline=line+'\t'+motif_dict[i]+'\n' 
    else: 
        outline=line+'\n' 
    outfile.write(outline) 
