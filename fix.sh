gzip -d *gz
for i in `seq 0 24`
do python ../unique_id.py het_$i\_deepLIFT_0.collapsed 
    bedtools sort -i het_$i\_deepLIFT_0.collapsed > het_$i\_deepLIFT_0.sorted
    bgzip het_$i\_deepLIFT_0.sorted
    tabix -p bed -f het_$i\_deepLIFT_0.sorted.gz 
    echo $i
done
