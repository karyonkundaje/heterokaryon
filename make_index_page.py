def make_index_page(datahubs, base_url, output_file):
    BROWSER_BASE_URL = \
        'http://epigenomegateway.wustl.edu/browser/?genome=hg19&datahub={}&tknamewidth=150'

    with open(output_file, 'w') as fp:
        fp.write('<html>\n')
        fp.write('<head>\n')
        fp.write('<title>DeepLIFT tracks</title>\n')
        fp.write('</head>\n')
        fp.write('<body>\n')
        fp.write('<ul>\n')
        for datahub_name, datahub_path in datahubs:
            datahub_url = urlparse.urljoin(base_url, datahub_path)
            browser_url = BROWSER_BASE_URL.format(datahub_url)
            fp.write('<li><a href="{}">{}</a></li>\n'.format(browser_url, datahub_name))
        fp.write('</ul>\n')
        fp.write('</body>\n')
        fp.write('</html>\n')
def main(): 
    datahub_name="heterokaryon_tracks.json" 
    datahub_path="http://mitra.stanford.edu/kundaje/annashch/heterokaryon_tracks.json" 
    

if __name__=="__main__": 
    main() 
