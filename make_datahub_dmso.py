import pdb 
import argparse
import glob
from itertools import product
import json
import os
import urlparse

# NOTE: JSON requires double-quotes; single-quotes are not valid
BIGWIG_JSON = """
    {
        "mode": "show",
        "name": "",
        "fixedscale":{"min": 0,"max": 10},
        "qtc": {
            "anglescale": 1,
            "height": 40,
            "pb": 128,
            "pg": 0,
            "pr": 0,
            "smooth": 3,
            "summeth": 2,
            "thtype": 0
        },
        "type": "bigwig",
        "url": ""
    }
"""

QCAT_SEQUENCE_JSON = """
    {
        "type": "quantitativeCategorySeries",
        "name": "",
        "height": 64,
        "url": "",
        "backgroundcolor": "#ffffff",
        "mode": "show",
        "categories": {
              "0": ["A", "#ff0000"],
              "1": ["C", "#0000ff"],
              "2": ["G", "#ffa500"],
              "3": ["T", "#00ff00"]
        }
    }
"""

QCAT_COMBINED_JSON = """
    {
        "type": "quantitativeCategorySeries",
        "name": "",
        "height": 64,
        "url": "",
        "backgroundcolor": "#ffffff",
        "mode": "show",
        "categories": {
              "0":["dnase", "#c0c0c0"],
              "1":["mnase", "#606060"],
              "2":["sequence", "#000099"]
        }
    }
"""
name_dict=dict() 
name_dict['0']='Control'
name_dict['1']='DMSO'
name_dict['2']='DMSO Up'
name_dict['3']='DMSO Down' 

metadata_dict=dict() 
metadata_dict['0']=[1,6,13] 
metadata_dict['1']=[2,7,13] 
metadata_dict['2']=[3,8,13] 
metadata_dict['3']=[4,9,13] 
metadata_dict['4']=[5,10,13] 
metadata_dict['5']=[1,7,11] 
metadata_dict['6']=[1,7,12] 
metadata_dict['7']=[2,8,11] 
metadata_dict['8']=[2,8,12] 
metadata_dict['9']=[1,8,11]
metadata_dict['10']=[1,8,12]
metadata_dict['11']=[3,9,11] 
metadata_dict['12']=[3,9,12] 
metadata_dict['13']=[2,9,11] 
metadata_dict['14']=[2,9,12]
metadata_dict['15']=[1,9,11]
metadata_dict['16']=[1,9,12]
metadata_dict['17']=[4,10,11]
metadata_dict['18']=[4,10,12]
metadata_dict['19']=[3,10,11]
metadata_dict['20']=[3,10,12]
metadata_dict['21']=[2,10,11]
metadata_dict['22']=[2,10,12]
metadata_dict['23']=[1,10,11]
metadata_dict['24']=[1,10,12]




def parse_args():
    # TODO: add BASE_URL and BASE_DATAHUB as actual options
    parser = argparse.ArgumentParser(
        description='Make Datahub JSONs for DeepLIFT tracks')

    parser.add_argument('tracks_dir',
                        help='Directory where all the trackfiles are '
                             ' (in subdirectories; see --singledir '
                             '  if this is the target directory)')

    parser.add_argument('--singledir',
                        help='tracks_dir directly contains the files',
                        action='store_true')
    parser.add_argument('--copy',
                        help='Also copy bigwig and hammock files '
                             'to destination directory.')
    args = parser.parse_args()

    BASE_DATAHUB = 'viz-base.json'
    BASE_URL = 'http://mitra.stanford.edu/kundaje/annashch/dmso/'

    setattr(args, 'base_datahub', BASE_DATAHUB)
    setattr(args, 'base_url', BASE_URL)

    return args


def make_datahub_from_directory(directory, base_datahub, base_url,
                                output_file):
    #MARKS = ['h3k4me1', 'h3k4me3', 'h3k27ac', 'ctcf','het']
    MARKS=['het'] 
    INPUTS=[str(i) for i in range(4)]
    #INPUTS = ['atac-dnase', 'atac-mnase', 'atac-sequence','0']
    TRACKS = ['deepLIFT']
    input2json = {
        'atac-dnase': BIGWIG_JSON,
        'atac-mnase': BIGWIG_JSON,
        'atac-sequence': QCAT_SEQUENCE_JSON,
        'combined': QCAT_COMBINED_JSON,
        '0': QCAT_SEQUENCE_JSON,
    }

    input2ext = {
        'atac-dnase': 'bw',
        'atac-mnase': 'bw',
        'atac-sequence': 'gz',
        'combined': 'gz',
        '0': 'gz',
    }

    datahub_json = json.load(open(base_datahub))
    datahub_name = os.path.basename(directory)

    for mark, track, input_type in product(MARKS, TRACKS, INPUTS):
        #ext = input2ext[input_type]
        ext='gz'
        track_files = sorted(glob.glob(
            os.path.join(directory,
                         '{}_{}_{}*.{}'.format(mark, input_type, track, ext))))
        print str(mark) 
        print str(input_type) 
        print str(track) 
        print str(ext) 
        try:
            #pdb.set_trace() 
            track_files = [track_files[0]]
        except IndexError:
            print('Warning: {} is missing files for mark {} input_type {}'
                  ' track {}'.format(directory, mark, input_type, track))
        for track_file in track_files:
            #track_json = json.loads(input2json[input_type])
            track_json = json.loads(QCAT_SEQUENCE_JSON)
            input_name = (input_type[5:] if input_type.startswith('atac-')
                          else input_type)
            track_json['name']=name_dict[input_name]
            #track_json['metadata']=metadata_dict[input_name] 
            track_json['url'] = urlparse.urljoin(base_url, track_file)
            datahub_json.append(track_json)

    full_output_file = os.path.join(directory, output_file)
    json.dump(datahub_json, open(full_output_file, 'w'), indent=4)
    return (datahub_name, full_output_file)


def make_index_page(datahubs, base_url, output_file):
    BROWSER_BASE_URL = \
        'http://epigenomegateway.wustl.edu/browser/?genome=hg19&datahub={}&tknamewidth=150'

    with open(output_file, 'w') as fp:
        fp.write('<html>\n')
        fp.write('<head>\n')
        fp.write('<title>DeepLIFT tracks</title>\n')
        fp.write('</head>\n')
        fp.write('<body>\n')
        fp.write('<ul>\n')
        for datahub_name, datahub_path in datahubs:
            datahub_url = urlparse.urljoin(base_url, datahub_path)
            browser_url = BROWSER_BASE_URL.format(datahub_url)
            fp.write('<li><a href="{}">{}</a></li>\n'.format(browser_url, datahub_name))
        fp.write('</ul>\n')
        fp.write('</body>\n')
        fp.write('</html>\n')


if __name__ == '__main__':
    args = parse_args()

    dirs_to_process = \
        ([args.tracks_dir] if args.singledir else
         [os.path.join(args.tracks_dir, subdir)
          for subdir in sorted(next(os.walk(args.tracks_dir))[1])])
    print str(dirs_to_process) 
    datahubs = [make_datahub_from_directory(directory, args.base_datahub,
                                            args.base_url, 'viz-dl.json')
                for directory in dirs_to_process]

    #make_index_page(datahubs, args.base_url,
    #                '{}-index.html'.format(args.tracks_dir))
