from __future__ import division;
from __future__ import print_function;
from __future__ import absolute_import;
import sys, os;
import matplotlib
matplotlib.use('Agg') 
from collections import OrderedDict, namedtuple, Counter;
import numpy as np;
sys.path.insert(0,"/users/avanti/caffe/python/")
#Import some general util stuff
scriptsDir = os.environ.get("UTIL_SCRIPTS_DIR");
if (scriptsDir is None):
    raise Exception("Please set environment variable UTIL_SCRIPTS_DIR to point to av_scripts");
sys.path.insert(0,scriptsDir);

scriptsDir = os.environ.get("ENHANCER_SCRIPTS_DIR");
if (scriptsDir is None):
    raise Exception("Please set environment variable ENHANCER_SCRIPTS_DIR to point to enhancer_prediction_code");
sys.path.insert(0,scriptsDir+"/featureSelector/deepLIFFT/");
import deepLIFTutils
scriptsDir = os.environ.get("KERAS_DIR");
if (scriptsDir is None):
    raise Exception("Please set environment variable KERAS_DIR");
import glob
import os,sys 
import compare_filters_to_known_motifs
from importDataPackage import importData
import pathSetter;
import util;
import fileProcessing as fp
import keras
from plottingUtilitiesPackage import matplotlibHelpers as mplh;
import criticalSubsetIdentification as csi

sys.path.insert(0,scriptsDir)
sys.path.insert(0,scriptsDir+"/featureSelector/deepLIFFT/kerasBasedBackprop");
from deepLIFTonGPU import ScoreTypes, Activations_enum, OutLayerInfo, getScoreFunc
np.random.seed(1234)

#Latest Example! 
print("starting to load model!") 
modelsDir="/srv/scratch/annashch/deeplearning/heterokaryon/modelsDir_runsDbPeytonModel_AllPairs"
modelWeights=modelsDir+"/bestModel_record_1_model_pZ9RQ_modelWeights.h5" 
modelYaml=modelsDir+"/bestModel_record_1_model_pZ9RQ_modelYaml.yaml" 
model = deepLIFTutils.loadKerasModel(modelWeights, modelYaml)
deepLIFTutils.meanNormaliseFirstConvLayerWeights(model);
print("loaded model!")

#load the data
print("loading data!") 
#yamlDir="/srv/scratch/annashch/deeplearning/heterokaryon/yaml"
yamlDir="/srv/scratch/annashch/deeplearning/heterokaryon/yaml_sliding_pangwei_allpairs"
trainData, validData, testData = importData.loadTrainTestValidFromYaml(
                                    yamlDir+"/features.yaml"
                                    ,yamlDir+"/labels.yaml"
                                    ,yamlDir+"/splits.yaml")
del trainData


data=testData
print(np.shape(data.X))


# In[8]:

#compile the deepLIFT scoring function
trackLayers = [model.layers[11],0] #final maxpooling layer! 
outputLayers = [OutLayerInfo(outLayNoAct=model.layers[-1],activation='sigmoid')]
import deepLIFTonGPU
scoreFunc = deepLIFTonGPU.getScoreFunc(model, trackLayers, outputLayers, 
                [ScoreTypes.deepLIFT]);


# In[9]:

#run the scoring function on the full validation set
allDLOutputOnData = deepLIFTutils.runScoreFuncOnData(batchSize=10
                                                          , data=[data.X]
                                                          , scoreFunc=scoreFunc
                                                          , progressUpdate=10000)


pwms=compare_filters_to_known_motifs.load_all_pwms() 
numtasks=np.shape(outputs)[1] 
print("numtasks:"+str(numtasks))
for i in range(numtasks): 
    print("Working on task:"+str(i))
    neuronOfInterest_idx = i #the neuron within the output layer you are interested in
    #figure out what the true positives are
    labelsForNeuronOfInterest = [x[neuronOfInterest_idx] for x in data.Y]
    outputsForNeuronOfInterest = [x[neuronOfInterest_idx] for x in outputs]



    threshold=0.5
    truePositiveIndices = csi.getTruePositiveIndicesAboveThreshold(
                                    outputs=outputsForNeuronOfInterest
                                    , trueLabels=labelsForNeuronOfInterest
                                    , thresholdProb=threshold
                                    , classifProb=threshold)

    print(len(truePositiveIndices))
    #Getting highly ranked true positives 
    ranking = sorted([(x,outputs[x][neuronOfInterest_idx]) for x in truePositiveIndices], key=lambda x: -x[1])
    print(ranking[:10])
    deepLIFTscoresForNeuronOfInterest = allDLOutputOnData[ScoreTypes.deepLIFT][0][0][neuronOfInterest_idx]
    print(deepLIFTscoresForNeuronOfInterest.shape)
    #noi = neuron of interest
    aggregatedTruePosScores_noi = deepLIFTutils.aggregateScoresOnIndices(
                                        deepLIFTscoresForNeuronOfInterest
                                        ,truePositiveIndices)
    print(str(aggregatedTruePosScores_noi.shape))
    sortedFilters_noi = sorted(enumerate(np.array(
                            np.squeeze(np.sum(aggregatedTruePosScores_noi,axis=-1))))
                                , key=lambda x: -x[1])
    mplh.plotHist([x[1] for x in sortedFilters_noi],bins=len(sortedFilters_noi), figsize=(7,4))
    matplotlib.pyplot.savefig("distribution_"+str(neuronOfInterest_idx)+'.png')


    # In[20]:
    numPositiveOutliers = 5
    numNegativeOutliers = 5
    printBias=False
    figSize=(7,4)
    weights, biases = model.layers[0].get_weights()
    outf=open('task_'+str(neuronOfInterest_idx)+"_motifs.txt",'w')
    myrank=-1 
    for (idx, score) in sortedFilters_noi[:numPositiveOutliers]:
        myrank+=1 
        deepLIFTutils.printConvolution(weights, biases, idx,printBias=printBias, addToTitle="score: "+str(score), figSize=figSize,outputFile="pos_task_"+str(neuronOfInterest_idx)+"_rank_"+str(myrank)+"_idx_"+str(idx)+".png")
        hits=compare_filters_to_known_motifs.get_pwm_matches_for_filter(weights[idx][0],pwms[1])
        outf.write('up'+'\t'+str(myrank))
        for i in hits: 
            outf.write('\t'+str(i))
        outf.write('\n')
    myrank=-1 
    for (idx, score) in sortedFilters_noi[-numNegativeOutliers:]:
        myrank+=1 
        deepLIFTutils.printConvolution(weights, biases, idx,printBias=printBias, addToTitle="score: "+str(score), figSize=figSize,outputFile="neg_task_"+str(neuronOfInterest_idx)+"_rank_"+str(myrank)+"_idx_"+str(idx)+".png")
        hits=compare_filters_to_known_motifs.get_pwm_matches_for_filter(weights[idx][0],pwms[1])
        outf.write('down'+'\t'+str(myrank))
        for i in hits: 
            outf.write('\t'+str(i))
        outf.write('\n')
