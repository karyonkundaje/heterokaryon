from PIL import Image
import os 
import sys 
import pdb
#import deepLIFT stuff
scriptsDir = os.environ.get("ENHANCER_SCRIPTS_DIR");
if (scriptsDir is None):
    raise Exception("Please set environment variable ENHANCER_SCRIPTS_DIR to point to enhancer_prediction_code");
sys.path.insert(0,scriptsDir+"/featureSelector/deepLIFFT/");
import criticalSubsetIdentification as csi
import compare_filters_to_known_motifs as cfkm
import deepLIFTutils
import matplotlib.pyplot as plt

name_dict=dict() 
'''
name_dict[0]='CC Peak Presenece'
name_dict[1]='3hr Peak Presence' 
name_dict[2]='16hr Peak Presence' 
name_dict[3]='48hr Peak Presence' 
name_dict[4]='H1 Peak Presence' 
name_dict[5]='3hr Upregulated from CC' 
name_dict[6]='3hr Downregulated from CC' 
name_dict[7]='16hr Upregulated from 3hr' 
name_dict[8]='16hr Downregulated from 3hr' 
name_dict[9]='16hr Upregulated from CC' 
name_dict[10]='16hr Downregulated from CC'
name_dict[11]='48hr Upregulated from 16hr' 
name_dict[12]='48hr Downregulated from 16hr'
name_dict[13]='48hr Upregulated from 3hr' 
name_dict[14]='48hr Downregulated from 3hr'
name_dict[15]='48hr Upregulated from CC' 
name_dict[16]='48hr Downregulated from CC'
name_dict[17]='H1 Upregulated from 48hr' 
name_dict[18]='H1 Downregulated from 48hr'
name_dict[19]='H1 Upregulated from 16hr' 
name_dict[20]='H1 Downregulated from 16hr'
name_dict[21]='H1 Upregulated from 3hr' 
name_dict[22]='H1 Downregulated from 3hr'
name_dict[23]='H1 Upregulated from CC' 
name_dict[24]='H1 Downregulated from CC'

name_dict[0]='Control'
name_dict[1]='DMSO'
name_dict[2]='DMSO Up'
name_dict[3]='DMSO Down' 
'''
name_dict[0]="3hr Up" 
name_dict[1]="3hr Down" 
name_dict[2]="3hr and H1 Up" 
name_dict[3]="3hr and H1 Down" 
name_dict[4]="16hr Up" 
name_dict[5]="16hr Down" 
name_dict[6]="16hr and H1 Up" 
name_dict[7]="16hr and H1 Down" 
name_dict[8]="48hr Up" 
name_dict[9]="48hr Down" 
name_dict[10]="48hr and H1 Up" 
name_dict[11]="48hr and H1 Down" 
def generate_motif_pngs(hits,maindir): 
    homer_pwms,encode_pwms,cisbp_pwms=cfkm.load_all_pwms() 
    plot_index=dict() 
    plot_index['homer']=dict() 
    plot_index['encode']=dict() 
    plot_index['cisbp']=dict() 
    curplot=0 
    if maindir.endswith('/')==False: 
        maindir=maindir+'/' 
    if not os.path.exists(maindir+'homer'): 
        os.makedirs(maindir+'homer') 
    if not os.path.exists(maindir+'encode'): 
        os.makedirs(maindir+'encode') 
    if not os.path.exists(maindir+'cisbp'):
        os.makedirs(maindir+'cisbp') 
    homer_dict=dict() #name to weights 
    encode_dict=dict() 
    cisbp_dict=dict() 
    for task in hits:
        for grammar_id in hits[task]: 
            motif_matches=hits[task][grammar_id]  
            for i in range(10): 
                cur_homer_name=motif_matches[0][i][3]                 
                cur_encode_name=motif_matches[1][i][3]
                cur_cisbp_name=motif_matches[2][i][3] 
                homer_dict[cur_homer_name]=1 
                encode_dict[cur_encode_name]=1 
                cisbp_dict[cur_cisbp_name]=1 
    #make images of representated TF's 
    for cur_pwm in homer_pwms: 
        if cur_pwm.name in homer_dict: 
            #check if the file aleady exists! 
            #if not os.path.exists(maindir+'homer/'+cur_pwm.name+'.png'): 
            #make picture! 
            plot_index['homer'][cur_pwm.name]=curplot
            cur_fig=deepLIFTutils.plot_bases(cur_pwm.weights,tuple([10,5]),show=False)
            plt.savefig(maindir+'homer/'+str(curplot)+'.png')
            plt.close('all') 
            curplot+=1 
    for cur_pwm in encode_pwms: 
        if cur_pwm.name in encode_dict:
            #make picture! 
            plot_index['encode'][cur_pwm.name]=curplot
            cur_fig=deepLIFTutils.plot_bases(cur_pwm.weights,tuple([10,5]),show=False)
            plt.savefig(maindir+'encode/'+str(curplot)+'.png')
            plt.close('all') 
            curplot+=1 
    for cur_pwm in cisbp_pwms: 
        if cur_pwm.name in cisbp_dict:
            plot_index['cisbp'][cur_pwm.name]=curplot
            cur_fig=deepLIFTutils.plot_bases(cur_pwm.weights,tuple([10,5]),show=False) 
            plt.savefig(maindir+'cisbp/'+str(curplot)+'.png') 
            plt.close('all') 
            curplot+=1 
    return plot_index
def sort_inputs_by_motif_match(hits,forward_figs,reverse_figs): 
    #TODO 
    return hits,forward_figs,reverse_figs 

def make_grammar_page(dirname,motif_matches,forward_fig,reverse_fig,task_index,grammar_index,task_name,plot_index): 
    #encode the images for direct embedding in html document 
    if dirname.endswith('/')==False: 
        dirname=dirname+'/' 
    output_image_forward='grammars_'+str(task_index)+'_'+str(grammar_index)+'.forward.png'
    output_image_reverse='grammars_'+str(task_index)+'_'+str(grammar_index)+'.reverse.png'
    forward_fig_uri=forward_fig.save(dirname+output_image_forward) 
    reverse_fig_uri=reverse_fig.save(dirname+output_image_reverse) 
    forward_img_tag='<img src="'+output_image_forward+'" style="height:50%;">\n'
    reverse_img_tag='<img src="'+output_image_reverse+'" style="height:50%;">\n'
    output_file=dirname+'grammars_'+str(task_index)+'_'+str(grammar_index)+'.html'
    with open(output_file, 'w') as fp:
        fp.write('<html>\n')
        fp.write('<head>\n')
        fp.write('<title>'+task_name+'</title>\n')
        fp.write('</head>\n')
        fp.write('<body>\n')
        fp.write('<table border="1" align="center">\n') 
        fp.write('<tr>\n')
        fp.write('<td> Forward </td>\n')
        fp.write('<td> Reverse Complement </td>\n') 
        fp.write('</tr>\n') 
        fp.write('<tr>\n')
        fp.write('<td>'+forward_img_tag+'</td>\n')
        fp.write('<td>'+reverse_img_tag+'</td>\n')
        fp.write('</tr>\n') 
        fp.write('</table>\n') 
        fp.write('<table border="1" align="center">\n') 
        fp.write('<tr>\n')
        fp.write('<td> HOMER correlation </td>\n')
        fp.write('<td> HOMER motif name </td>\n')
        fp.write('<td> HOMER motif PWM </td>\n')
        fp.write('<td> ENCODE correlation </td>\n') 
        fp.write('<td> ENCODE motif name </td>\n') 
        fp.write('<td> ENCODE motif PWM </td>\n') 
        fp.write('<td> CISBP correlation </td>\n') 
        fp.write('<td> CISBP motif name </td>\n') 
        fp.write('<td> CISBP motif PWM </td>\n') 
        fp.write('</tr>\n')
        for i in range(10): 
            cur_homer=motif_matches[0][i] 
            cur_encode=motif_matches[1][i] 
            cur_cisbp=motif_matches[2][i]
            fp.write('<tr>\n') 
            fp.write('<td>'+str(cur_homer[0])+'</td>\n')
            fp.write('<td>'+str(cur_homer[3].replace('\t',''))+'</td>\n')
            img_name=plot_index['homer'][cur_homer[3]]
            img_tag='<img src="../homer/'+str(img_name)+'.png" style="height:20%;"/>'     
            fp.write('<td>'+img_tag+'</td>\n')
            #link to the homer image 
            fp.write('<td>'+str(cur_encode[0])+'</td>\n')
            fp.write('<td>'+str(cur_encode[3].replace('\t',''))+'</td>\n')
            img_name=plot_index['encode'][cur_encode[3]]
            img_tag='<img src="../encode/'+str(img_name)+'.png" style="height:20%;"/>'     
            fp.write('<td>'+img_tag+'</td>\n')
            #link to the encode image 
            fp.write('<td>'+str(cur_cisbp[0])+'</td>\n')
            fp.write('<td>'+str(cur_cisbp[3].replace('\t',''))+'</td>\n')
            img_name=plot_index['cisbp'][cur_cisbp[3]]
            img_tag='<img src="../cisbp/'+str(img_name)+'.png" style="height:20%;"/>'     
            fp.write('<td>'+img_tag+'</td>\n')
            #link to the cisbp image 
            fp.write('</tr>\n') 
        fp.write('</table>\n') 
        fp.write('</body>\n')
        fp.write('</html>\n')
    return output_file 

def generate_html(maindir,hits,forward_figs,reverse_figs): 
    if not os.path.exists(maindir): 
        os.makedirs(maindir) 
    #create pngs of motif weights! 
    plot_index=generate_motif_pngs(hits,maindir) 
    page_titles=dict() 
    max_grammars=0 
    for task_index in hits.keys():
        dirname=maindir+'/task'+str(task_index)
        if not os.path.exists(dirname): 
            os.makedirs(dirname) 
        page_titles[task_index]=[] 
        cur_hits=hits[task_index] 
        cur_forward=forward_figs[task_index] 
        cur_reverse=reverse_figs[task_index] 
        num_grammars=len(cur_hits.keys()) 
        if num_grammars > max_grammars: 
            max_grammars=num_grammars 
        for grammar_index in range(num_grammars): 
            page_title=make_grammar_page(dirname,cur_hits[grammar_index],cur_forward[grammar_index],cur_reverse[grammar_index],task_index,grammar_index,name_dict[task_index],plot_index)
            page_titles[task_index].append(page_title) 
    #generate main page for the grammar 
    with open(maindir+'/main.html', 'w') as fp:
        fp.write('<html>\n')
        fp.write('<head>\n')
        fp.write('<title>Heterokaryon Grammars</title>\n')
        fp.write('</head>\n')
        fp.write('<body>\n')
        fp.write('<table border="1" align="center">\n') 
        fp.write('<tr>\n')
        for task in name_dict: 
            task_name=name_dict[task] 
            fp.write('<td> '+str(task_name)+' </td>\n') 
        fp.write('</tr>\n') 
        for grammar_index in range(max_grammars): 
            fp.write('<tr>\n')
            for task_index in range(0,12):#,len(name_dict.keys())): 
                #insert forward images in table 
                if len(forward_figs[task_index]) > grammar_index: 
                    output_image_forward='task'+str(task_index)+'/grammars_'+str(task_index)+'_'+str(grammar_index)+'.forward.png'
                    output_image_file='task'+str(task_index)+'/grammars_'+str(task_index)+'_'+str(grammar_index)+'.html'
                    forward_img_tag='<a href="'+output_image_file+'"><img src="'+output_image_forward+'" style="height:10%;"/></a>\n'                    
                    fp.write('<td>'+forward_img_tag+'</td>\n')
                    #insert figure with hyperlink!
                else: 
                    #insert blank! 
                    fp.write('<td></td>\n')
            fp.write('</tr>\n')
        fp.write('</table>\n') 
        fp.write('</body>\n')
        fp.write('</html>\n')
