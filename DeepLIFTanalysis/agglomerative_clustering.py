# This script shows end-to-end grammar analysis of Chromputer sequence-only
# models
#
# TODOS:
#
# 1) look at distribution of seqlet max cc
#
# **swap contribution to positive neuron with difference between positive
# contribution and negative contribution**
#
# **scaling up:**
#
# for every TF peak, compute scores for every true positive mark overlapping
# that peak, filter out from this set all seqlets with score below the mean
# score - run motif discovery on the full set of DeepLIFT scores
#
# **motif discovery pipline:**
#
# **grammar detection:**
# run motif discovery for 7bp and 15bp seqlets, 15bp motifs containing pairs of
# 7bp motifs are grammars. For remaining 7bp motifs, check offsets between
# them, if spacing distribution is sufficiently contrained then its a grammar.
# For the reamining motifs, we look for indirect interactions in downstream
# analysis of other modes.
import matplotlib
matplotlib.use('Agg')

import argparse
from itertools import chain
import logging
from operator import itemgetter
import os
import pickle
import sys

import numpy as np
from matplotlib import pyplot as plt
# get_ipython().magic(u'matplotlib inline')
import seaborn as sns
from scipy.io import loadmat
from sklearn.cluster import MiniBatchKMeans
from scipy.cluster.hierarchy import fcluster

from fastcluster import linkage
from kmedoids import kmedoids

from keras.models import model_from_json
import theano
scriptsDir = os.environ.get("UTIL_SCRIPTS_DIR");
if (scriptsDir is None):
    raise Exception("Please set environment variable UTIL_SCRIPTS_DIR to point to av_scripts");
sys.path.insert(0,scriptsDir);
import pathSetter;
import util;
import sklearn; 
import fileProcessing as fp
from plottingUtilitiesPackage import matplotlibHelpers as mplh;

#import deepLIFT stuff
scriptsDir = os.environ.get("ENHANCER_SCRIPTS_DIR");
if (scriptsDir is None):
    raise Exception("Please set environment variable ENHANCER_SCRIPTS_DIR to point to enhancer_prediction_code");
sys.path.insert(0,scriptsDir+"/featureSelector/deepLIFFT/");
import criticalSubsetIdentification as csi
import deepLIFTutils
sys.path.insert(0,scriptsDir+"/featureSelector/deepLIFFT/kerasBasedBackprop");
from deepLIFTonGPU import ScoreTypes, Activations_enum, OutLayerInfo, getScoreFunc



def plot_clustering_heatmap(merged_grammars_cc, agglomerated_indices):
    indices_list = [list(indices_set)
                    for indices_set in agglomerated_indices]
    ordered_idx = \
        list(chain.from_iterable(indices_list))
    sns.heatmap(merged_grammars_cc[ordered_idx, :][:, ordered_idx],
                xticklabels=False, yticklabels=False)
    return


def agglomerative_clustering(grammars, gradient_track, cc_threshold):
    grammars_list = grammars.values()
    indices_list = [set([i]) for i in range(len(grammars_list))]
    while True:
        # The PerPosNormFuncs are used for ensuring that the corrrelations are
        # between -1 and 1
        grammars_cc = csi.getCorrelationMatrix(
            grammars_list,
            subtracksToInclude=[gradient_track],
            accountForRevComp=True,
            numThreads=1,
            secondsBetweenUpdates=6,
            xcorBatchSize=None,
            smallerPerPosNormFuncs=[util.PERPOS_NORMFUNC.oneOverTwoNorm],
            largerPerPosNormFuncs=[util.PERPOS_NORMFUNC.oneOverTwoNorm])

        np.fill_diagonal(grammars_cc, 0)
        max_grammars_cc = np.max(grammars_cc)

        print("max cc: ", max_grammars_cc)
        if max_grammars_cc < cc_threshold:
            break

        max_cc_idx1, max_cc_idx2 = \
            np.unravel_index(np.argmax(grammars_cc), grammars_cc.shape)

        merged_grammar = grammars_list[max_cc_idx1].merge(
            grammars_list[max_cc_idx2],
            subtracksToInclude=[gradient_track],
            subtrackNormaliseFunc=util.CROSSC_NORMFUNC.meanAndSdev,
            normaliseFunc=util.CROSSC_NORMFUNC.none,
            smallerPerPosNormFuncs=[util.PERPOS_NORMFUNC.oneOverTwoNorm],
            largerPerPosNormFuncs=[util.PERPOS_NORMFUNC.oneOverTwoNorm],
            revComp=True)

        removeset = {max_cc_idx1, max_cc_idx2}
        merged_indices = \
            indices_list[max_cc_idx1].union(indices_list[max_cc_idx2])
        indices_list = [indices_set
                        for i, indices_set in enumerate(indices_list)
                        if i not in removeset]
        indices_list.append(merged_indices)
        grammars_list = [grammar
                         for i, grammar in enumerate(grammars_list)
                         if i not in removeset]
        grammars_list.append(merged_grammar)

    return grammars_list, indices_list


