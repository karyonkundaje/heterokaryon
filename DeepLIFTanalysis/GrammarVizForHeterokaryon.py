from __future__ import division;
from __future__ import print_function;
from __future__ import absolute_import;
import sys, os;
from collections import OrderedDict, namedtuple;
import numpy as np;

#Import some general util stuff
scriptsDir = os.environ.get("UTIL_SCRIPTS_DIR");
if (scriptsDir is None):
    raise Exception("Please set environment variable UTIL_SCRIPTS_DIR to point to av_scripts");
sys.path.insert(0,scriptsDir);
import pathSetter;
import util;
import fileProcessing as fp
from plottingUtilitiesPackage import matplotlibHelpers as mplh;

#import deepLIFT stuff
scriptsDir = os.environ.get("ENHANCER_SCRIPTS_DIR");
if (scriptsDir is None):
    raise Exception("Please set environment variable ENHANCER_SCRIPTS_DIR to point to enhancer_prediction_code");
sys.path.insert(0,scriptsDir+"/featureSelector/deepLIFFT/");
import criticalSubsetIdentification as csi
import deepLIFTutils
sys.path.insert(0,scriptsDir+"/featureSelector/deepLIFFT/kerasBasedBackprop");
from deepLIFTonGPU import ScoreTypes, Activations_enum, OutLayerInfo, getScoreFunc

#Make sure the directory is set to import the lab's version of keras
scriptsDir = os.environ.get("KERAS_DIR");
if (scriptsDir is None):
    raise Exception("Please set environment variable KERAS_DIR");
sys.path.insert(0,scriptsDir)
# In[119]:
#load the data
import pickle
import criticalSubsetIdentification as csi
from generate_html import * 
from kmedoids import kmedoids 
from agglomerative_clustering import * 
os.environ['CUDA_DEVICE']="7" #you should set this if you plan to do xcor on the GPU                                                                                                             

hit_dict=dict() 
forward_fig_dict=dict() 
reverse_fig_dict=dict() 
for cur_task_id in range(43): 
    pickle_name="/srv/scratch/annashch/heterokaryon/DeepLIFTanalysis/remapped.grammars."+str(cur_task_id)+".pkl"
    data=open(pickle_name,'rb')
    grammars=pickle.load(data)
    grammarsCorrMat=pickle.load(data)
    data.close() 
    trackNamesToPrint=[csi.coreDeepLIFTtrackName, "sequence", "gradients"]

    #coarse initializations for k-medoids clustering 
    KMEANS_NUM_CLUSTERS=20 #number of initial coarse clusters using k-means 
    KMEANS_INITS=10 #number of initializations for k-means 
    subtracksToInclude=["gradients"]
    grammarsCorrMat = csi.getCorrelationMatrix(
                            grammars
                            , normaliseFunc=util.CROSSC_NORMFUNC.meanAndTwoNorm
                            , subtracksToInclude=subtracksToInclude
                            , accountForRevComp=True
                            , numThreads=1
                            , secondsBetweenUpdates=3
                            , xcorBatchSize=10 #set this to something other than None to do xcor on GPU                                                                                                          
                            #, subtrackNormaliseFunc=util.CROSSC_NORMFUNC.perPositionRange                                                                                                                       
                            )
    #k-medoids clustering 
    max_grammars_cc=np.max(grammarsCorrMat,axis=1)
    len(max_grammars_cc)
    max_cc=grammarsCorrMat.max() 
    min_cc=grammarsCorrMat.min() 
    if abs(max_cc)<1e-5: 
        max_cc=1e-5
    if abs(min_cc)<1e-5: 
        min_cc=1e-5

    corr_mat_diag=grammarsCorrMat.diagonal() 
    assert 0 <=min_cc and max_cc <=1
    grammarsCorrMat*=-1 
    grammarsCorrMat+=1 
    np.fill_diagonal(grammarsCorrMat,0)

    labels, _, _ = kmedoids(grammarsCorrMat, n_clusters=KMEANS_NUM_CLUSTERS,
                                n_init=KMEANS_INITS)

    merged_grammars, original_shifts = csi.merge_grammars(labels,grammars,subtracksToInclude=subtracksToInclude,
            normaliseFunc=util.CROSSC_NORMFUNC.meanAndTwoNorm,
            accountForRevComp=True)
    # trim grammars        
    TRIM_FRAC=0.3
    trimmingFunc = csi.TrimArrayColumnsToNumUnderlyingObs(TRIM_FRAC)
    merged_grammars = csi.adjustGrammarsUsingTrimmingCriterion(merged_grammars, trimmingFunc=trimmingFunc)
    print("finished!")

    # perform agglomorative clustering         
    agglomerated_grammars, agglomerated_indices = agglomerative_clustering(
        merged_grammars, subtracksToInclude[0], cc_threshold=0.9)

    print(str(merged_grammars.values()[0]))
    subtracksToInclude=["gradients"]
    merged_grammars_cc = csi.getCorrelationMatrix(
        merged_grammars.values(),
        subtracksToInclude=subtracksToInclude,
        accountForRevComp=True,
        numThreads=1,
        secondsBetweenUpdates=3,
        xcorBatchSize=None,
        smallerPerPosNormFuncs=[util.PERPOS_NORMFUNC.oneOverTwoNorm],
        largerPerPosNormFuncs=[util.PERPOS_NORMFUNC.oneOverTwoNorm])

    #plot_clustering_heatmap(merged_grammars_cc, agglomerated_indices)

    def convert_pwm_to_bits(pwm, pseudocount=1e-6):
        # WARNING: Modifies PWM in place                                                                                                                                                                         
        assert pwm.shape[0] == 4
        pwm *= 2 + (pwm * np.log2(pwm + pseudocount)).sum(axis=0, keepdims=True)


    # In[133]:

    for grammar in agglomerated_grammars:
        convert_pwm_to_bits(grammar.getNormalisedDataTrack('sequence'))


    # In[134]:
    matplotlib.use('Agg') 
    print("len agglomerated grammars:"+str(len(agglomerated_grammars)))
    cur_index=0 
    forward_figs=[] 
    reverse_figs=[] 
    for grammar in agglomerated_grammars:
        print("agglomerated grammar index:"+str(cur_index))
        cur_fig=csi.printGrammar(grammar, trackNamesToPrint=[csi.Grammar.coreDeepLIFTtrackName, "sequence", "gradients"])
        cur_fig_rev=csi.printGrammar(grammar.getRevCompGrammar(), trackNamesToPrint=[csi.Grammar.coreDeepLIFTtrackName, "sequence","gradients"])
        forward_figs.append(csi.fig2img(cur_fig))
        reverse_figs.append(csi.fig2img(cur_fig_rev))
        cur_index+=1



    # In[101]:
    agglomerated_dict=dict(zip(range(len(agglomerated_grammars)),agglomerated_grammars))
    hits=csi.compareToKnownMotifs_returndata(agglomerated_dict, trackNameForComparison="gradients")
    hit_dict[cur_task_id]=hits
    forward_fig_dict[cur_task_id]=forward_figs
    reverse_fig_dict[cur_task_id]=reverse_figs


#dirname="/srv/scratch/annashch/heterokaryon/DeepLIFTanalysis/grammar_html"
dirname="grammar_html"
generate_html(dirname,hit_dict,forward_fig_dict,reverse_fig_dict)



