### Script to compare convolutional filters to known motifs
### Peyton Greenside (adapted from CSFoo)
### 12/18/15
##################################################################

### Imports
import glob
import os, sys

import numpy as np
from scipy.signal import correlate2d
import pandas as pd

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import pickle
sys.path.insert(0,"/users/pgreens/git/keras")
scriptsDir = os.environ.get("UTIL_SCRIPTS_DIR");
if (scriptsDir is None):
    raise Exception("Please set environment variable UTIL_SCRIPTS_DIR");
sys.path.insert(0,scriptsDir);
import pathSetter;
import fileProcessing as fp;
import keras;
scriptsDir = os.environ.get("ENHANCER_SCRIPTS_DIR");
if (scriptsDir is None):
    raise Exception("Please set environment variable ENHANCER_SCRIPTS_DIR");
sys.path.insert(0,scriptsDir+"/featureSelector/deepLIFFT/");
from importDataPackage import importData;
# from modelSaver import normaliseWeightsForSequenceConvolutionalLayer
# import layerers
from plottingUtilitiesPackage import matplotlibHelpers as mplh;
yaml_dir='/users/pgreens/git/deeplearning/av_deeplearn_framework/yaml_150K_negatives/'
import scipy.misc
import h5py
import operator

from pyDNAbinding.plot import plot_bases 

### Define classes and functions
##################################################################
class PWM(object):
    def __init__(self, weights, name=None, threshold=None):
        self.weights = weights
        self.name = name
        self.threshold = threshold

    @staticmethod
    def from_homer_motif(motif_file):
        with open(motif_file) as fp:
            header = fp.readline().strip().split('\t')
            name = header[1]
            threshold = float(header[2])
            weights = np.loadtxt(fp)

        return PWM(weights, name, threshold)

    @staticmethod
    def get_encode_pwms(motif_file):
        pwms = []
        with open(motif_file) as fp:
            line = fp.readline().strip()
            while True:
                if line == '':
                    break

                header = line
                weights = []
                while True:
                    line = fp.readline()
                    if line == '' or line[0] == '>':
                        break
                    weights.append(map(float, line.split()[1:]))
                pwms.append(PWM(np.array(weights), header))
        return pwms

    @staticmethod
    def from_cisbp_motif(motif_file):
        name = os.path.basename(motif_file)
        with open(motif_file) as fp:
            _ = fp.readline()
            weights = np.loadtxt(fp)[:, 1:]
        return PWM(weights, name)


# Get the maximal and minimal cross correlation between PWM and Filter
# Notes: can play with fillvalue of 0 or 0.25
def max_min_cross_corr(pwm, conv_filter):
    if conv_filter.shape[1] != 4:
        conv_filter = conv_filter.T
    assert conv_filter.shape[1] == 4
    assert pwm.shape[1] == 4

    corr = correlate2d(pwm, conv_filter, mode='same', fillvalue=0.)
    # we are only interested in the part where the 'letter' axes are aligned,
    # and the correlation is over the position axis only, which is in the 2nd
    # column
    allowed_corr = corr[:, 1]
    max_corr = np.max(allowed_corr)
    min_corr = np.min(allowed_corr)

    # max_pos (and min_pos) relates to the alignment between the pwm and the
    # conv_filter as follows - more generally,
    #
    # Position floor(w / 2) - i maps to position 0 on the (padded) PWM
    #
    # where w = width of conv filter, and i is the position given by best_pos
    # (or more generally, the index into allowed_corr). If a negative position
    # is obtained, that means that position 0 on the conv_filter maps to a
    # later position on the PWM.
    max_pos = np.argmax(allowed_corr)
    min_pos = np.argmin(allowed_corr)

    return ((max_corr, max_pos), (min_corr, min_pos))

# Use to get PWM matches for a given filter 
# Normalize filter by max/min activation
def get_pwm_matches_for_filter(conv_filter, pwms, topk=10, bottomk=None):

    min_activation = conv_filter.min(axis=0).sum()
    max_activation = conv_filter.max(axis=0).sum()
    activation_range = max_activation - min_activation

    def norm_cc(cc):
        return (cc - min_activation) / activation_range

    hits = []

    for idx, pwm in enumerate(pwms):
        (max_cc, max_pos), (min_cc, min_pos) = \
            max_min_cross_corr(pwm.weights, conv_filter)

        hits.append((norm_cc(max_cc), max_pos, idx, pwm.name))
        if bottomk != None:
            hits.append((norm_cc(min_cc), min_pos, idx, pwm.name))

        hits.sort(reverse=True)

    if bottomk == None:
        return hits[:topk]
    else:
        return hits[:topk] + hits[-bottomk:]


# Use to get filter matches for a set of PWM
# Standardize the mean and stddev of all filters to compare (could do other ways, but seems to work for current matches)
def get_filter_matches_for_pwm(conv_filter, bias, pwms, topk=10):

    hits = []

    # Normalize mean and standard deviation
    conv_filter = (conv_filter - np.mean(conv_filter))/np.std(conv_filter)

    for idx, pwm in enumerate(pwms):

        (max_cc, max_pos), (min_cc, min_pos) = \
            max_min_cross_corr(pwm.weights, conv_filter)
        (max_cc_rc, max_pos_rc), (min_cc, min_pos) = \
            max_min_cross_corr(pwm.weights, conv_filter[::-1, ::-1])

        if max_cc_rc > max_cc:
            # print 'RC greater'
            hits.append((max_cc_rc+0, max_pos_rc, idx, pwm.name))
        else:
            hits.append((max_cc+0, max_pos, idx, pwm.name))
        hits.sort(reverse=True)

    else:
        return hits[:topk]


### Load Motifs
##################################################################

# Load and return encode, homer and cisb PWMs
def load_all_pwms():
    motifs_path = '/software/homer/default/motifs/*.motif'
    model_file = '/mnt/lab_data/kundaje/users/nasa/dnase-mnase/gdl_folds/GM12878_dnase_2k/chromputer_sequence_0/23137/sequence-0.tar.gz'

    homer_pwms = [PWM.from_homer_motif(motif_file)
            for motif_file in glob.glob(motifs_path)]

    encode_motifs = '/mnt/data/ENCODE/bindingSites/encodeMotifs/dec2013/motifs/motifs.txt'
    encode_pwms = PWM.get_encode_pwms(encode_motifs)

    cisbp_motifs_path = '/users/csfoo/cisbp-pwms/*.txt'
    cisbp_annots = pd.read_table("/mnt/lab_data/kundaje/users/pgreens/projects/hematopoiesis/data/cisbp_TF_ids.txt")
    cisbp_pwms = [PWM.from_cisbp_motif(motif_file) 
                  for motif_file in glob.glob(cisbp_motifs_path)]

    id_list = cisbp_annots.ix[:,"Motif_ID"].tolist()
    for pwm in cisbp_pwms:
        if pwm.name.split('.txt')[0] in id_list:
            ind = id_list.index(pwm.name.split('.txt')[0])
            pwm.name = cisbp_annots.ix[ind,"TF_Name"]
    return (homer_pwms, encode_pwms, cisbp_pwms)

# ### Load Model
##################################################################

#  From random initialization
# initial_weights_file='/mnt/lab_data/kundaje/users/pgreens/projects/hematopoiesis/models/saved_weights/dec_16_yaml_150K_neg_no_hema_Leuk_75M_fullbasset_10x5x1learnrate_noweightnorm.h5'
# final_weights_file='/users/pgreens/git/deeplearning/av_deeplearn_framework/saved_models/dec_16_yaml_150K_neg_no_hema_Leuk_75M_fullbasset_10x5x1learnrate_noweightnorm_epoch_12_modelWeights.h5'
#  Jittered positives diff peaks
# final_weights_file='/users/pgreens/git/deeplearning/av_deeplearn_framework/saved_models/dec_19_150K_neg_no_hema_diff_peaks_Leuk_75M_jitter_BassetModel_10xlearnrate_noweightnorml1_epoch_8_modelWeights.h5'

# def loadWeightsForLayer(h5file, layerIdx):
#     f = h5file
#     print f.keys()
#     g=f['layer_{}'.format(layerIdx)] #gets the first layer
#     weights = [g['param_{}'.format(p)] for p in range(g.attrs['nb_params'])]
#     return [np.asarray(x) for x in weights]

# # convolutionalWeights_init, convolutionalBiases_init = loadWeightsForLayer(h5py.File(initial_weights_file), 0)   
# convolutionalWeights_final, convolutionalBiases_final = loadWeightsForLayer(h5py.File(final_weights_file), 0)   


# ### Compare Filter Motifs to Model
# ##################################################################

# for ind in range(convolutionalWeights_final.shape[0]):
#     conv_filter = convolutionalWeights_final[ind,0,:,:]
#     # homer_hits = get_motif_matches(convfilter, homer_pwms)
#     encode_hits = get_motif_matches(conv_filter, encode_pwms, bottomk=None)
#     if encode_hits[0][0]>0.85:
#         print ind
#         print encode_hits[0]

# homer_hits = get_motif_matches(convolutionalWeights_final, homer_pwms)
# encode_hits = get_motif_matches(filters, encode_pwms)
# cisbp_hits = get_motif_matches(filters, cisbp_pwms)


### For each ChIP in K562 use, CS's tool to find the most similar filters and then plot those
##################################################################

# Load weights for layer 
def loadWeightsForLayer(h5file, layerIdx):
    f = h5file
    print f.keys()
    g=f['layer_{}'.format(layerIdx)] #gets the first layer
    weights = [g['param_{}'.format(p)] for p in range(g.attrs['nb_params'])]
    return [np.asarray(x) for x in weights]

# Find the PWMs corresponding to chip name, find filters that best match those PWMs 
# Return filter numbers, top scores, and top PWMs they correspond to (comma-separated)
def get_filter_numbers_corresponding_to_pwms(chip_name, pwms, final_weights_file, num_top_filters=1):
    # get all the filter
    chip_len_name = len(chip_name)+2
    chip_pwms = [el for el in pwms if el.name[1:chip_len_name]==chip_name.upper()+'_' or el.name[1:chip_len_name]==chip_name.capitalize()+'_']
    if len(chip_pwms)==0:
        return (None,None,None)
    # for these PWMs, rank the filters and their scores
    convolutionalWeights_final, convolutionalBiases_final = loadWeightsForLayer(h5py.File(final_weights_file), 0) 
    best_pwm_scores = {}
    best_pwm_names = {}
    for f in range(convolutionalWeights_final.shape[0]):
        conv_filter = convolutionalWeights_final[f,0,:,:]
        # encode_hits = get_motif_matches(conv_filter, chip_pwms, bottomk=None)
        encode_hits = get_filter_matches_for_pwm(conv_filter, convolutionalBiases_final[f], chip_pwms, topk=10)
        # best_pwm_scores[f] = np.max([el[0] for el in encode_hits if el[0]<1]+[0])
        best_pwm_scores[f] = np.max([el[0] for el in encode_hits ])
        best_pwm_names[f] = [el[3] for el in encode_hits if el[0]==best_pwm_scores[f] ][0]
    sorted_best_pwm_scores = sorted(best_pwm_scores.items(), key=operator.itemgetter(1), reverse=True)
    filter_nums = ','.join([str(el[0]) for el in sorted_best_pwm_scores[0:num_top_filters]])
    top_scores = ','.join([str('%.3f'%el[1]) for el in sorted_best_pwm_scores[0:num_top_filters]])
    top_names = ','.join([best_pwm_names[el[0]] for el in sorted_best_pwm_scores[0:num_top_filters]])
    return (filter_nums, top_scores, top_names)

# Visualize filter and save to plot_name with provided output_prefix and title
def plot_filter_motif(motif_filter, output_prefix, title, plot_name):
    
    # motif_filter = filters[0, ...].squeeze()

    assert len(motif_filter.shape) == 2
    assert 4 in motif_filter.shape

    # May need to transpose as plot_bases takes a N x 4 array
    if motif_filter.shape[0] == 4:
        motif_filter = motif_filter.T

    motif_filter -= motif_filter.mean(axis=1, keepdims=True)
    motif_filter *= 10

    plt.close()
    plot_bases(motif_filter, 'Weight (x 10)')
    plt.title('{}'.format(title))
    plt.savefig(plot_name)



### Correct inconsistent labels
##################################################################

def correct_inconsistent_labels(bed_file, new_bed_file, codes=['000', '011', '101']):
    bed = pd.read_table(bed_file, header=0, sep="\t")
    labels = bed.columns[5:20].tolist()
    pair_labels = bed.columns[21::].tolist()
    print bed.ix[:,5::].sum().sum()
    for pair in pair_labels:
        if '|' in pair:
            continue
        label1 = pair.split('v')[0]
        label2 = pair.split('v')[1]
        for ind in xrange(bed.shape[0]):
            # Set all non-present pairs to non-different
            if '000' in codes:
                if bed.ix[ind,label1]==0 and bed.ix[ind,label2]==0 and bed.ix[ind,pair]!=0:
                    bed.ix[ind,pair]=0
            # Set all non-present/present pairs to non-different
            if '011' in codes: 
                if bed.ix[ind,label1]==0 and bed.ix[ind,label2]==1 and bed.ix[ind,pair]!=1:
                    bed.ix[ind,pair]=1
            # Set all present/non-present pairs to non-different
            if '101' in codes:
                if bed.ix[ind,label1]==1 and bed.ix[ind,label2]==0 and bed.ix[ind,pair]!=1:
                    bed.ix[ind,pair]=1
    print bed.ix[:,5::].sum().sum()
    bed.to_csv(new_bed_file, sep="\t", header=True, index=False)


if __name__ == "__main__":

    ### Iterate through every chip and closest approximation
    CHIP_PATH = '/mnt/data/ENCODE/peaks_spp/mar2012/distinct/idrOptimalBlackListFilt/'
    PLOT_PATH = '/mnt/lab_data/kundaje/users/pgreens/projects/hematopoiesis/plots/'
    SCRIPT_PATH = '/users/pgreens/git/disease_projects/hematopoiesis/'
    final_weights_file='/users/pgreens/git/deeplearning/av_deeplearn_framework/saved_models/jan_4_150K_neg_no_hema_nondiff_plus_diff_peaks_Leuk_75M_jitter_600filters_BassetModel_600filters_10xlearnrate_noweightnorml1_epoch_8_modelWeights.h5'
    convolutionalWeights_final, convolutionalBiases_final = loadWeightsForLayer(h5py.File(final_weights_file), 0) 
    plot=True
    # for chip_exp in [el for el in os.listdir(CHIP_PATH) if 'K562' in el if 'StdAln' in el]: # or 'Gm12878'
    cell='Gm12878' # "K562"
    for chip_exp in [el for el in os.listdir(CHIP_PATH) if cell in el if 'StdAln' in el]: # or 'Gm12878'
        chip_file = CHIP_PATH+chip_exp
        chip_name = chip_exp.split(cell)[1].split('StdAln')[0]
        (chip_filters, top_scores, top_names) = get_filter_numbers_corresponding_to_pwms(chip_name, encode_pwms, final_weights_file, num_top_filters=10)
        if chip_filters==None:
            continue
        title_line = "Top Scores: {0}".format(top_scores)
        best_score = float(top_scores.split(',')[0])
        best_name = top_names.split(',')[0]
        if plot==True:
            print 'Plotting the best match'
            best_filter_number = int(chip_filters.split(',')[0])
            plot_name='{0}best_match_{1}_filter_{2}.png'.format(PLOT_PATH, chip_name, best_filter_number)
            plot_title = 'Best Match for {0}, filter number {1}\n best scores {2} best match {3}'.format(chip_name, best_filter_number, best_score, best_name)
            plot_filter_motif(convolutionalWeights_final[best_filter_number,0,:,:], None, plot_title, plot_name=plot_name)
        if best_score > 0.7:
            print 'Calculating AUC distribution'
            print chip_name
            print best_score
            command="""
            python {0}compare_filters_to_chipseq_peaks.py --pickled-model /users/pgreens/git/deeplearning/av_deeplearn_framework/saved_models/jan_4_150K_neg_no_hema_nondiff_plus_diff_peaks_Leuk_75M_jitter_600filters_BassetModel_600filters_10xlearnrate_noweightnorml1_epoch_8_modelPickle.pkl \
            --bed-file /mnt/lab_data/kundaje/users/pgreens/projects/hematopoiesis/data/boosting_input/atac_peaks_Leuk_75M_macsqval5_deseq_pval_extended_jitter15_w_hb_p10_150000rand_abs_with_nondiff_peaks.txt \
            --activation-layer 1 \
            --chip-file {1} \
            --chip-name {2} \
            --num-best-chip-peaks 10000 \
            --chip-filters {3} \
            --plot-path /mnt/lab_data/kundaje/users/pgreens/projects/hematopoiesis/plots/ \
            --title-line "{4}" \
            """.format(SCRIPT_PATH, chip_file, chip_name+'_'+cell+'_bestactivation', chip_filters, title_line)
            os.system(command)



    ### Clean up data
    ### README 
    # clean000 is converting all individual 0 and 0 to differential of 0
    # clean011 is converting all individual 0 and 1 to differential of 1
    # clean101 is converting all individual 1 and 0 to differential of 1
    codes=['000', '011', '101']
    BOOST_PATH='/mnt/lab_data/kundaje/users/pgreens/projects/hematopoiesis/data/boosting_input/'
    bed_file = '{0}atac_peaks_Leuk_75M_macsqval5_deseq_pval_extended_jitter15_w_hb_p10_150000rand_abs_with_nondiff_peaks.txt'.format(BOOST_PATH)
    new_bed_file='{0}atac_peaks_Leuk_75M_macsqval5_deseq_pval_extended_jitter15_w_hb_p10_150000rand_abs_with_nondiff_peaks_clean_{1}.txt'.format(BOOST_PATH, '_'.join(codes))
    correct_inconsistent_labels(bed_file, new_bed_file,codes=codes)


    ### GATA EXPLORATION
    gata_pwms=[el for el in cisbp_pwms if 'Gata' in el.name or 'GATA' in el.name]
    # (chip_filters, top_scores, top_names) = get_filter_numbers_corresponding_to_pwms('GATA', gata_pwms, final_weights_file, num_top_filters=1)
    best_pwm_scores = {}
    best_pwm_names = {}
    for f in range(convolutionalWeights_final.shape[0]):
        conv_filter = convolutionalWeights_final[f,0,:,:]
        # encode_hits = get_motif_matches(conv_filter, chip_pwms, bottomk=None)
        encode_hits = get_filter_matches_for_pwm(conv_filter, convolutionalBiases_final[f], gata_pwms, topk=10)
        best_pwm_scores[f] = np.max([el[0] for el in encode_hits ])
        best_pwm_names[f] = [el[3] for el in encode_hits if el[0]==best_pwm_scores[f] ][0]
    sorted_best_pwm_scores = sorted(best_pwm_scores.items(), key=operator.itemgetter(1), reverse=True)
    filter_nums = ','.join([str(el[0]) for el in sorted_best_pwm_scores[0:num_top_filters]])
    top_scores = ','.join([str('%.3f'%el[1]) for el in sorted_best_pwm_scores[0:num_top_filters]])
    top_names = ','.join([best_pwm_names[el[0]] for el in sorted_best_pwm_scores[0:num_top_filters]])

    ### CALL COMPARE TO CHIP-Seq

    # SCRIPT_PATH=/users/pgreens/git/disease_projects/hematopoiesis/
    # CHIP_PATH=/mnt/data/ENCODE/peaks_spp/mar2012/distinct/idrOptimalBlackListFilt/

    # gata1chip=$CHIP_PATH'wgEncodeSydhTfbsK562Gata1UcdAlnRep0.bam_VS_wgEncodeSydhTfbsK562InputUcdAlnRep1.bam.regionPeak.gz'
    # gata2chip=$CHIP_PATH'wgEncodeSydhTfbsK562Gata2UcdAlnRep0.bam_VS_wgEncodeSydhTfbsK562InputUcdAlnRep1.bam.regionPeak.gz'
    # gata_filters=0,30
    # chip_name=GATA1_K562
    # pkl_model=/users/pgreens/git/deeplearning/av_deeplearn_framework/saved_models/jan_18_diff_and_present_peaks_datasetLeuk_75M_logpval5_diffmethoddeseq_top100000_peaksize600_jitter15_noGCmatch_splitDiffTasks_BassetModel_10xlearnrate_noweightnorml1_epoch_14_modelPickle.pkl
    # bed_file=/mnt/lab_data/kundaje/users/pgreens/projects/hematopoiesis/data/deep_learning_peaks/diff_and_present_peaks_datasetLeuk_75M_logpval5_diffmethoddeseq_top100000_peaksize600_jitter15_noGCmatch_splitDiffTasks
    # python $SCRIPT_PATH"compare_filters_to_chipseq_peaks.py" --pickled-model $pkl_model \
    # --bed-file $bed_file --activation-layer 1 \
    # --chip-file $gata1chip --chip-name $chip_name \
    # --chip-filters $gata_filters \
    # --plot-path /mnt/lab_data/kundaje/users/pgreens/projects/hematopoiesis/plots/ \
    # --title-line $chip_name --run-all\

