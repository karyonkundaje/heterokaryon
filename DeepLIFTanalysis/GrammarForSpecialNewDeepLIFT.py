from __future__ import division;
from __future__ import print_function;
from __future__ import absolute_import;
import sys, os;
from collections import OrderedDict, namedtuple;
import pdb
import numpy as np;
import matplotlib; 
import heapq; 
import time; 
import h5py;  
import pickle 
matplotlib.use('Agg') 
scriptsDir = os.environ.get("UTIL_SCRIPTS_DIR");
if (scriptsDir is None):
    raise Exception("Please set environment variable UTIL_SCRIPTS_DIR to point to av_scripts");
sys.path.insert(0,scriptsDir);
import pathSetter;
import util;
import sklearn; 
import fileProcessing as fp
from plottingUtilitiesPackage import matplotlibHelpers as mplh;
#import deepLIFT stuff
scriptsDir = os.environ.get("ENHANCER_SCRIPTS_DIR");
if (scriptsDir is None):
    raise Exception("Please set environment variable ENHANCER_SCRIPTS_DIR to point to enhancer_prediction_code");
sys.path.insert(0,scriptsDir+"/featureSelector/deepLIFFT/");
sys.path.insert(0,scriptsDir+"/featureSelector/deepLIFFT/kerasBasedBackprop");
from deepLIFTonGPU import ScoreTypes, Activations_enum, OutLayerInfo, getScoreFunc
import criticalSubsetIdentification as csi
#Make sure the directory is set to import the lab's version of keras
scriptsDir = os.environ.get("KERAS_DIR");
if (scriptsDir is None):
    raise Exception("Please set environment variable KERAS_DIR");
sys.path.insert(0,scriptsDir)
from importDataPackage import importData
import deepLIFTutils
import deepLIFTonGPU

#load the data
#data_file=h5py.File("/srv/scratch/annashch/deeplearning/heterokaryon/inputs/newpeaks.long.hdf5","r")
data_file=h5py.File("/srv/scratch/annashch/deeplearning/heterokaryon/inputs/specialized.hdf5","r") 
data_X=np.asarray(data_file["testX"])
data_Y=np.asarray(data_file["testY"])
print("loaded data") 

#load the model 
current_task=int(sys.argv[1]) 
current_gpu=sys.argv[2] 
#modelsDir="/srv/scratch/annashch/deeplearning/heterokaryon/modelsDir_runsDbPeytonModel_AllPairs_2/"
modelsDir="/srv/scratch/annashch/deeplearning/heterokaryon/modelsDir_runsDbPeytonModel_AllPairs_2/"
modelWeights=modelsDir+"/record_94_model_AQ8OM_modelWeights.h5" 
modelYaml=modelsDir+"/record_94_model_AQ8OM_modelYaml.yaml"
model = deepLIFTutils.loadKerasModel(modelWeights, modelYaml)
deepLIFTutils.meanNormaliseFirstConvLayerWeights(model);
print("loaded model") 
#compile the scoring function 
trackLayers = [0]
outputLayers = [OutLayerInfo(outLayNoAct=model.layers[-2],activation='sigmoid')]
scoreFunc = deepLIFTonGPU.getScoreFunc(model, trackLayers, outputLayers,[ScoreTypes.deepLIFT_rawContrib, ScoreTypes.sensitivity]);
print("compiled scoring function") 
#define some constants 
trackLayerIdx = 0
outputLayerIdx = 0; #idx of the output layer in outputLayers argument to compiling the scoreFunc above
thresholdProb=0.5 
batchsize=1000
criticalSubsetThresholdProb = 1.0

#Get all the outputs on the data set:
outputs = deepLIFTutils.getSequentialModelLayerOutputs(
                                        model
                                        , inputDatas=[data_X]
                                        , layerIdx=-1
                                        , batchSize=500)
#iterate through all the tasks!! 
grammar_dict=dict() #task_number->grammar_crossentropy matrix
all_grammars=np.array([]) 
all_grammar_indices=np.array([]) 
all_grammar_scores=np.array([]) 
#outf=h5py.File("heterokaryon.grammars.hdf5","w") 
numtasks=data_Y.shape[1] 
#ds=outf.create_dataset("numtasks",data=numtasks)
for neuronOfInterest_idx in range(current_task,current_task+1): 
    outf=open('specialized.grammars.'+str(neuronOfInterest_idx)+'.pkl','wb') 
    print("task:"+str(neuronOfInterest_idx)) 
    outputs_singleNeuron = [x[neuronOfInterest_idx] for x in outputs];
    trueLabels_singleNeuron = [x[neuronOfInterest_idx] for x in data_Y]
    truePositiveIndices=csi.getTruePositiveIndicesAboveThreshold(
                            outputs=outputs_singleNeuron
                            , trueLabels=trueLabels_singleNeuron
                            , thresholdProb=thresholdProb)


    data_x_tp=data_X[truePositiveIndices]
    data_y_tp=data_Y[truePositiveIndices]
    #process batches of data at a time to help with memory constraint 
    start_index=0 
    data_x_tp_length=len(data_x_tp)
    end_index=min([start_index+batchsize, data_x_tp_length-1])
    while end_index < data_x_tp_length: 
        print("start_index:"+str(start_index)+"--end_index:"+str(end_index)) 
        allDLOutputOnData = deepLIFTutils.runScoreFuncOnData(batchSize=500
                                                             , data=data_x_tp[start_index:end_index]
                                                             , scoreFunc=scoreFunc
                                                             , progressUpdate=500)
        
        #pdb.set_trace() 
        dLRawContribs_singleNeuron = allDLOutputOnData[ScoreTypes.deepLIFT_rawContrib][outputLayerIdx][trackLayerIdx][neuronOfInterest_idx];
        segmentIdentifier = csi.FixedWindowAroundPeaks(
                        slidingWindowForMaxSize=6
                        , flankToExpandAroundPeakSize=5
                        , excludePeaksWithinWindow=20
                        , ratioToTopPeakToInclude=0.5
                        , maxSegments=5);
        grammars, grammarIndices = csi.getGrammars(
                                rawDeepLIFTContribs=dLRawContribs_singleNeuron
                                , indicesToGetGrammarsOn=None
                                , outputsBeforeActivation=None
                                , activation=None
                                , thresholdProb=criticalSubsetThresholdProb
                                , segmentIdentifier=segmentIdentifier
                                , numThreads=1
                                , secondsBetweenUpdates=1)
        #pdb.set_trace() 
        grammar_scores=[np.sum(grammars[i].summedCoreDeepLIFTtrack) for i in range(len(grammars))]
        dLGradients_singleNeuron = allDLOutputOnData[ScoreTypes.sensitivity][outputLayerIdx][trackLayerIdx][neuronOfInterest_idx];
        dLGradients_singleNeuron = np.squeeze(dLGradients_singleNeuron,axis=1);
        for dataToAugmentWith,name,pseudocount,revCompFunc in [(np.squeeze(data_x_tp[start_index:end_index], axis=1)
                                                                , "sequence"
                                                                , 0.25
                                                                , csi.dnaRevCompFunc)
                                                               ,(dLGradients_singleNeuron
                                                                 , "gradients"
                                                                 , 0.0
                                                                 , csi.dnaRevCompFunc)]:
            csi.augmentGrammarsWithData(grammars, fullDataArr=dataToAugmentWith
                                        , keyName=name
                                        , pseudocount=pseudocount
                                        , revCompFunc=revCompFunc
                                        , indicesToSubset=None)

        #Merge with the existing subset of grammars
        all_grammars=np.append(all_grammars,grammars) 
        all_grammar_indices=np.append(all_grammar_indices,grammarIndices) 
        all_grammar_scores=np.append(all_grammar_scores,grammar_scores)
        #filter to top 1000
        top_indices=heapq.nlargest(1000,range(len(all_grammar_scores)),all_grammar_scores.take)
        all_grammars=all_grammars[top_indices] 
        all_grammar_indices=all_grammar_indices[top_indices] 
        all_grammar_scores=all_grammar_scores[top_indices] 

        #increment the batch 
        start_index=min([start_index+batchsize, data_x_tp_length])
        end_index=min([start_index+batchsize, data_x_tp_length])
        
    #continue processing the grammars for this particular task 
    #xcor code can currently only handle equally sized seqlets so filter out the few seqlets that were at the
    #edge and are thus smaller
    normalGrammarLen = np.max([x.summedCoreDeepLIFTtrack.shape[1] for x in grammars])
    grammars = [x for x in grammars if x.summedCoreDeepLIFTtrack.shape[1]==normalGrammarLen]
    
    grammarsSubset=grammars
    accountForRevComp=True
    #subtracksToInclude represents the set of subtracks to do the
    #cross correlation based on. You could in theory do the
    #cross correlation based on some of the augmented data tracks
    #as well
    #subtracksToInclude=[csi.Grammar.coreDeepLIFTtrackName]
    subtracksToInclude=["gradients"]
    os.environ['CUDA_DEVICE']=current_gpu #you should set this if you plan to do xcor on the GPU
    grammarsCorrMat = csi.getCorrelationMatrix(
                        grammarsSubset
                        , normaliseFunc=util.CROSSC_NORMFUNC.meanAndTwoNorm
                        , subtracksToInclude=subtracksToInclude
                        , accountForRevComp=accountForRevComp
                        , numThreads=1
                        , secondsBetweenUpdates=3
                        , xcorBatchSize=10 #set this to something other than None to do xcor on GPU
                        #, subtrackNormaliseFunc=util.CROSSC_NORMFUNC.perPositionRange
                        )
    #store grammars & grammarsCorrMat to HDF5 file! 
    pickle.dump(grammars,outf,-1) 
    pickle.dump(grammarsCorrMat,outf,-1)
    outf.close()     
    #ds=outf.create_dataset("grammars_"+str(neuronOfInterest_idx),data=grammars)
    #ds=outf.create_dataset("grammarsCorrMat_"+str(neuronOfInterest_idx),data=grammarsCorrMat)
    #outf.flush() 

