import keras 
import sys 

modelsDir="/srv/scratch/annashch/deeplearning/heterokaryon/modelsDir_runsDbPeytonModel1"
modelWeights = modelsDir+"/bestModel_record_1_model_MWIHY_modelWeights.h5"
modelYaml = modelsDir+"/bestModel_record_1_model_MWIHY_modelYaml.yaml" 

from keras.models import model_from_yaml
model = model_from_yaml(open(modelYaml).read())
'''
model.load_weights(modelWeights)
from importDataPackage import importData
reload(importData)
yamlDir="/srv/scratch/annashch/deeplearning/heterokaryon/yaml_sliding_pangwei"
trainData, validData, testData = importData.loadTrainTestValidFromYaml(
                                    yamlDir+"/features.yaml"
                                    ,yamlDir+"/labels.yaml"
                                    ,yamlDir+"/splits.yaml")
del trainData
'''
