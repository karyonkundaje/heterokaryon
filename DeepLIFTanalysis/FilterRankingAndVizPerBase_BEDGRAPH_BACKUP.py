from __future__ import division;
from __future__ import print_function;
from __future__ import absolute_import;
import sys, os;
import matplotlib
matplotlib.use('Agg') 
from collections import OrderedDict, namedtuple, Counter;
import numpy as np;
sys.path.insert(0,"/users/avanti/caffe/python/")
#Import some general util stuff
scriptsDir = os.environ.get("UTIL_SCRIPTS_DIR");
if (scriptsDir is None):
    raise Exception("Please set environment variable UTIL_SCRIPTS_DIR to point to av_scripts");
sys.path.insert(0,scriptsDir);

scriptsDir = os.environ.get("ENHANCER_SCRIPTS_DIR");
if (scriptsDir is None):
    raise Exception("Please set environment variable ENHANCER_SCRIPTS_DIR to point to enhancer_prediction_code");
sys.path.insert(0,scriptsDir+"/featureSelector/deepLIFFT/");
import deepLIFTutils
scriptsDir = os.environ.get("KERAS_DIR");
if (scriptsDir is None):
    raise Exception("Please set environment variable KERAS_DIR");
import glob
import os,sys 
import compare_filters_to_known_motifs
from importDataPackage import importData
import pathSetter;
import util;
import fileProcessing as fp
import keras
from plottingUtilitiesPackage import matplotlibHelpers as mplh;
import criticalSubsetIdentification as csi
import pdb 

sys.path.insert(0,scriptsDir)
sys.path.insert(0,scriptsDir+"/featureSelector/deepLIFFT/kerasBasedBackprop");
from deepLIFTonGPU import ScoreTypes, Activations_enum, OutLayerInfo, getScoreFunc
np.random.seed(1234)

#Latest Example! 
print("starting to load model!") 
#modelsDir="/srv/scratch/annashch/deeplearning/heterokaryon/modelsDir_runsDbPeytonModel_AllPairs"
modelsDir="/srv/scratch/annashch/deeplearning/heterokaryon/modelsDir_runsDbPeytonModel_AllPairs_2"
modelWeights=modelsDir+"/record_59_model_8d7fv_modelWeights.h5"
modelYaml=modelsDir+"/record_59_model_8d7fv_modelYaml.yaml"
model = deepLIFTutils.loadKerasModel(modelWeights, modelYaml)
deepLIFTutils.meanNormaliseFirstConvLayerWeights(model);
print("loaded model!")

#load the data
print("loading data!") 
#yamlDir="/srv/scratch/annashch/deeplearning/heterokaryon/yaml"
#yamlDir="/srv/scratch/annashch/deeplearning/heterokaryon/yaml_sliding_pangwei_allpairs_roi"
yamlDir="/srv/scratch/annashch/deeplearning/heterokaryon/yaml_idr_sliced_win50"
trainData, validData, testData,evalData = importData.loadTrainTestValidFromYaml(
                                    yamlDir+"/features.yaml"
                                    ,yamlDir+"/labels.yaml"
                                    ,yamlDir+"/splits.yaml")
del trainData
del evalData


data=testData
print(np.shape(data.X))
outputs = deepLIFTutils.getSequentialModelLayerOutputs(
                                        model
                                        , inputDatas=[data.X]
                                        , layerIdx=-1
                                        , batchSize=10
                                        , progressUpdate=10000)
outputs=np.array(outputs)


# In[8]:

#compile the deepLIFT scoring function
trackLayers = [model.layers[-7],0] #final maxpooling layer! 
outputLayers = [OutLayerInfo(outLayNoAct=model.layers[-1],activation='sigmoid')]
import deepLIFTonGPU
scoreFunc = deepLIFTonGPU.getScoreFunc(model, trackLayers, outputLayers, 
                [ScoreTypes.deepLIFT]);


# In[9]:

#run the scoring function on the full validation set
allDLOutputOnData = deepLIFTutils.runScoreFuncOnData(batchSize=10
                                                          , data=[data.X]
                                                          , scoreFunc=scoreFunc
                                                          , progressUpdate=10000)


pwms=compare_filters_to_known_motifs.load_all_pwms() 
numtasks=np.shape(outputs)[1] 
print("numtasks:"+str(numtasks))
#bedgraph=open('bedgraph_heterokaryon.roi.txt','r').read().split('\n') 
bedgraph=open('allPeaks.merged','r').read().split('\n') 
bedgraph_dict=dict() 
while '' in bedgraph: 
    bedgraph.remove('') 
for i in range(numtasks): 
    print("Working on task:"+str(i))
    bedgraph_dict[i]=dict() 
    neuronOfInterest_idx = i #the neuron within the output layer you are interested in
    #figure out what the true positives are
    labelsForNeuronOfInterest = [x[neuronOfInterest_idx] for x in data.Y]
    outputsForNeuronOfInterest = [x[neuronOfInterest_idx] for x in outputs]
    threshold=0.5
    trueNegativeIndices = csi.getTruePositiveIndicesAboveThreshold(
                                    outputs=outputsForNeuronOfInterest
                                    , trueLabels=labelsForNeuronOfInterest
                                    , thresholdProb=threshold
                                    , classifProb=threshold)
    #Getting highly ranked true positives 
    ranking = sorted([(x,outputs[x][neuronOfInterest_idx]) for x in trueNegativeIndices], key=lambda x: -x[1])
    deepLIFTscoresForNeuronOfInterest = allDLOutputOnData[ScoreTypes.deepLIFT][0][1][neuronOfInterest_idx]
    for entry in ranking: 
        exampleIndex=entry[0]
        bedgraph_entry=bedgraph[exampleIndex].split('\t')  
        scores=np.squeeze(100*deepLIFTscoresForNeuronOfInterest[exampleIndex]) #4X1000 
        max_scores=np.amax(scores,axis=0) 
        min_scores=np.amin(scores,axis=0) 
        chrom=bedgraph_entry[0] 
        startpos=int(bedgraph_entry[1])
        #endpos=int(bedgraph_entry[2])
        if chrom not in bedgraph_dict[i]: 
            bedgraph_dict[i][chrom]=dict() 
        for p in range(len(max_scores)):
            max_val=max_scores[p] 
            min_val=min_scores[p] 
            if abs(max_val)> abs(min_val): 
                cur_val=max_val
            else: 
                cur_val=min_val 
            curpos=startpos+p 
            if curpos not in bedgraph_dict[i][chrom]: 
                bedgraph_dict[i][chrom][curpos]=cur_val 
            elif abs(cur_val)> abs(bedgraph_dict[i][chrom][curpos]): 
                bedgraph_dict[i][chrom][curpos]=cur_val 
                
#Write bedgraph tracks 
for i in range(numtasks): 
    outf=open('deeplift_scores_task'+str(i)+'.idr.tn.bedgraph','w')
    for chrom in bedgraph_dict[i]: 
        for pos in bedgraph_dict[i][chrom]: 
            outf.write(chrom+'\t'+str(pos-1)+'\t'+str(pos)+'\t'+str(bedgraph_dict[i][chrom][pos])+'\n')

