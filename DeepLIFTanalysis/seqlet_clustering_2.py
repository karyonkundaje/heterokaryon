# This script shows end-to-end grammar analysis of Chromputer sequence-only
# models
#
# TODOS:
#
# 1) look at distribution of seqlet max cc
#
# **swap contribution to positive neuron with difference between positive
# contribution and negative contribution**
#
# **scaling up:**
#
# for every TF peak, compute scores for every true positive mark overlapping
# that peak, filter out from this set all seqlets with score below the mean
# score - run motif discovery on the full set of DeepLIFT scores
#
# **motif discovery pipline:**
#
# **grammar detection:**
# run motif discovery for 7bp and 15bp seqlets, 15bp motifs containing pairs of
# 7bp motifs are grammars. For remaining 7bp motifs, check offsets between
# them, if spacing distribution is sufficiently contrained then its a grammar.
# For the reamining motifs, we look for indirect interactions in downstream
# analysis of other modes.
import matplotlib
matplotlib.use('Agg')

import argparse
from itertools import chain
import logging
from operator import itemgetter
import os
import pickle
import sys

import numpy as np
from matplotlib import pyplot as plt
# get_ipython().magic(u'matplotlib inline')
import seaborn as sns
from scipy.io import loadmat
from sklearn.cluster import MiniBatchKMeans
from scipy.cluster.hierarchy import fcluster

from fastcluster import linkage
from kmedoids import kmedoids

from keras.models import model_from_json
import theano

from skcaffe.util import mkdir_p

# Setup environment for av_scripts and DeepLIFT imports
import keras
scriptsDir = '/srv/scratch/csfoo/projects/enhancer_prediction_code'
os.environ['ENHANCER_SCRIPTS_DIR'] = scriptsDir
os.environ['KERAS_DIR'] = os.path.dirname(keras.__file__)
os.environ['UTIL_SCRIPTS_DIR'] = '/srv/scratch/csfoo/projects/experimental/pwmkeras/av_scripts/' # nopep8

scriptsDir = os.environ.get("ENHANCER_SCRIPTS_DIR")
if (scriptsDir is None):
    raise Exception('Please set environment variable ENHANCER_SCRIPTS_DIR to'
                    'point to enhancer_prediction_code')
os.sys.path.insert(0, scriptsDir+"/featureSelector/deepLIFFT/")

import criticalSubsetIdentification as csi
import deepLIFTutils
import miscDeepLIFTutils
from deepLIFTonGPU import ScoreTypes
import util

# Setup logging
log_formatter = \
    logging.Formatter('%(levelname)s:%(asctime)s:%(name)s] %(message)s')

logger = logging.getLogger('seqlet_clustering')
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
handler.setFormatter(log_formatter)
logger.setLevel(logging.INFO)
logger.addHandler(handler)
logger.propagate = False

# Magic numbers and constants here (that are not in args)
BLOB_DIMS = 4

SEQUENCE_CONV_LAYER_NAME = 'sequence-conv1'
# Only consider (positive) examples that have posterior at least this
LABEL_POSTERIOR_CUTOFF = 0.5

# For seqlet identification:
# Minimum ratio of other DeepLIFT score peaks to top scoring seqlet peak
DEEPLIFT_RATIO_TO_TOP_PEAK = 0.5
# Maximum seqlets to identify per input sequence
DEEPLIFT_MAX_SEQLETS = 10

# For seqlet clustering:
# Number of threads to use for identifying seqlets
NUM_THREADS = 8
# Maximum number of seqlets to consider in clustering
MAX_SEQLETS = 20000
# Batchsize for GPU cross-correlation computation
# (reduce this if getting GPU memory errors)
XCOR_GPU_BATCHSIZE = 200
# Number of initial coarse clusters using k-means
KMEANS_NUM_CLUSTERS = 100
# Number of initializations for k-means
KMEANS_INITS = 10
# Minimum fraction of total observations required to support a column in the
# merged seqlet. Used to trim out 'noise' columns from the edges
TRIM_FRAC = 0.3


def parse_args():
    parser = argparse.ArgumentParser(description='DeepLIFT seqlet analysis')

    parser.add_argument('model_json', help='Keras model json')
    parser.add_argument('model_weights', help='Keras model weights (HDF5)')
    parser.add_argument('input_data', help='mat file containing data')
    parser.add_argument('input_layer',
                        help='Input layer to compute DeepLIFT scores for.')

    parser.add_argument('seqlet_size',
                        help='Size of seqlets (recommended: 7-20)',
                        type=int)
    parser.add_argument('seqlet_flank',
                        help='Size of flanks to exclude neighboring seqlet '
                             'hits; normally should be at least seqlet_size',
                        type=int)

    parser.add_argument('--output_dir', help='Output prefix for plots')

    parser.add_argument('--chromputer_mark',
                        help='Mark name (for chromputer models)')

    parser.add_argument('--tf_name',
                        help='TF to restrict to (in mat file)')
    #parser.add_argument('--output_fc_layer',
    #                    help='Fully-connected layer for output you want to '
    #                         'compute DeepLIFT scores for.')
    #parser.add_argument('--output_name',
    #                    help='Output name in input_data mat file')
    #parser.add_argument('--output_layer',
    #                    help='Output layer that you want DeepLIFT scores for.')

    parser.add_argument('--save',
                        help='Save intermediate results of '
                             'agglomerative clustering')

    parser.add_argument('--verbose', help='Verbose mode', action='store_true')

    return parser.parse_args()

# define model and weights files, input data, target mark and tf,
# and relevant layers
#fname = '/srv/scratch/projects/chromputer/models/states-ctcf-marks.sequence-0/model.json'
#weights_fname = '/srv/scratch/projects/chromputer/models/states-ctcf-marks.sequence-0/model_weights.hdf5'
#input_data = '/srv/scratch/projects/chromputer/models/states-ctcf-marks.sequence-0/inputs_all.mat'
#TF_NAME = 'Pu1_GM12878'
#MARK_OUTPUT_NAME = 'ip3-h3k27ac_softmax'
#MARK_LABEL_NAME = 'h3k27ac'
#INPUT_LAYER_NAME = 'atac-sequence'
#MARK_FC_LAYER_NAME = 'ip3-h3k27ac'
#seqlet_size = 7
#seqlet_flank_size = 7


# define utility functions
def expand_dims_blob(arr, target_num_axes=BLOB_DIMS):
    # Reshapes arr, adds dims after the first axis
    assert len(arr.shape) <= BLOB_DIMS
    extra_dims = target_num_axes - len(arr.shape)
    new_shape = (arr.shape[0],) + (1,)*extra_dims + tuple(arr.shape[1:])
    return arr.reshape(new_shape)


def set_plot_style():
    matplotlib.rcParams['axes.grid'] = False
    matplotlib.rcParams['axes.facecolor'] = 'white'


def convert_pwm_to_bits(pwm, pseudocount=1e-6):
    # WARNING: Modifies PWM in place
    assert pwm.shape[0] == 4
    pwm *= 2 + (pwm * np.log2(pwm + pseudocount)).sum(axis=0, keepdims=True)


def names_from_mark(model, mark):
    softmax_layer, = [layer for layer in model.nodes.keys()
                      if layer.endswith('{}_softmax'.format(mark))]
    ip_layer = softmax_layer.split('_')[0]
    return mark, mark + '_loss', ip_layer


def get_indices_subset(data, mark_label_name, pred_layer_name, tf_name=None,
                       label_posterior_cutoff=LABEL_POSTERIOR_CUTOFF):
    # TODO: make this work for the state label that is multiclass
    # Works only for binary labels
    # get set of positive indices
    true_labels = getattr(data['labels'], mark_label_name)
    positive_indices = set(np.nonzero(true_labels == 1)[0])

    # get set of predicted positive indices
    model_preds = getattr(data['predictions'], pred_layer_name)
    pred_positive_indices = \
        set(np.nonzero(model_preds > label_posterior_cutoff)[0])

    indices_to_use = \
        positive_indices.intersection(pred_positive_indices)

    if tf_name is not None:
        # get set of indices of true positives overlapping tf
        factor_labels = getattr(data['factors'], tf_name)
        tf_indices = set(np.nonzero(factor_labels == 1)[0])
        indices_to_use = tf_indices.intersection(indices_to_use)

    return indices_to_use


def plot_clustering_heatmap(merged_grammars_cc, agglomerated_indices):
    indices_list = [list(indices_set)
                    for indices_set in agglomerated_indices]
    ordered_idx = \
        list(chain.from_iterable(indices_list))
    sns.heatmap(merged_grammars_cc[ordered_idx, :][:, ordered_idx],
                xticklabels=False, yticklabels=False)
    return


def agglomerative_clustering(grammars, gradient_track, cc_threshold):
    grammars_list = grammars.values()
    indices_list = [set([i]) for i in range(len(grammars_list))]
    while True:
        # The PerPosNormFuncs are used for ensuring that the corrrelations are
        # between -1 and 1
        grammars_cc = csi.getCorrelationMatrix(
            grammars_list,
            subtracksToInclude=[gradient_track],
            accountForRevComp=True,
            numThreads=1,
            secondsBetweenUpdates=6,
            xcorBatchSize=None,
            smallerPerPosNormFuncs=[util.PERPOS_NORMFUNC.oneOverTwoNorm],
            largerPerPosNormFuncs=[util.PERPOS_NORMFUNC.oneOverTwoNorm])

        np.fill_diagonal(grammars_cc, 0)
        max_grammars_cc = np.max(grammars_cc)

        print("max cc: ", max_grammars_cc)
        if max_grammars_cc < cc_threshold:
            break

        max_cc_idx1, max_cc_idx2 = \
            np.unravel_index(np.argmax(grammars_cc), grammars_cc.shape)

        merged_grammar = grammars_list[max_cc_idx1].merge(
            grammars_list[max_cc_idx2],
            subtracksToInclude=[gradient_track],
            subtrackNormaliseFunc=util.CROSSC_NORMFUNC.meanAndSdev,
            normaliseFunc=util.CROSSC_NORMFUNC.none,
            smallerPerPosNormFuncs=[util.PERPOS_NORMFUNC.oneOverTwoNorm],
            largerPerPosNormFuncs=[util.PERPOS_NORMFUNC.oneOverTwoNorm],
            revComp=True)

        removeset = {max_cc_idx1, max_cc_idx2}
        merged_indices = \
            indices_list[max_cc_idx1].union(indices_list[max_cc_idx2])
        indices_list = [indices_set
                        for i, indices_set in enumerate(indices_list)
                        if i not in removeset]
        indices_list.append(merged_indices)
        grammars_list = [grammar
                         for i, grammar in enumerate(grammars_list)
                         if i not in removeset]
        grammars_list.append(merged_grammar)

    return grammars_list, indices_list


# Tracknames
def deeplift_trackname(basename):
    return basename + '_rawDeepLIFT'


def gradient_trackname(basename):
    return basename + '_gradients'

SEQUENCE_TRACKNAME = 'sequence'


# *** MAIN SCRIPT ***
args = parse_args()
output_name = args.chromputer_mark
output_prefix = os.path.join(args.output_dir, output_name)
mkdir_p(args.output_dir)

# load model
logger.info('Loading model and weights...')
model = model_from_json(open(args.model_json, 'r').read())
model.load_weights(args.model_weights)
mark_label_name, pred_layer_name, ip_layer = \
    names_from_mark(model, output_name)

# load data
logger.info('Loading data from mat file...')
data = loadmat(args.input_data, squeeze_me=True, struct_as_record=False)

input_data = {}
for field in data['input_data']._fieldnames:
    input_field = 'atac-{}'.format(field)
    field_data = getattr(data['input_data'], field)
    input_data[input_field] = expand_dims_blob(field_data)
    logger.info('Shape of input {}: {}'.format(input_field, field_data.shape))

# get region indices where true positive mark predictions overlap target TF
indices_to_use = get_indices_subset(
    data, mark_label_name, pred_layer_name, args.tf_name)
logger.info('Number of examples in subset: {}'.format(len(indices_to_use)))

# normalize sequence conv layer
deepLIFTutils.meanNormaliseConvLayerWithName(model, SEQUENCE_CONV_LAYER_NAME)

# score true positive mark regions overlapping tf with DeepLIFT
logger.info('Computing DeepLIFT scores...')
deepLIFT_scores = miscDeepLIFTutils.getDeepLIFTscoresForSegmentIdentification(
    model=model,
    inputDatas=input_data,
    modelFcLayerName=ip_layer,
    trackLayerNames=[args.input_layer],
    indicesToScore=indices_to_use,
    scoreType=ScoreTypes.deepLIFT_rawContrib)

logger.info('Computing gradients...')
gradients_only = miscDeepLIFTutils.getDeepLIFTscoresForSegmentIdentification(
    model=model,
    inputDatas=input_data,
    modelFcLayerName=ip_layer,
    trackLayerNames=[args.input_layer],
    indicesToScore=indices_to_use,
    scoreType=ScoreTypes.sensitivity)

gradients_only[args.input_layer] = (
    gradients_only[args.input_layer] -
    np.mean(gradients_only[args.input_layer], axis=2)[:, None, :])

# shutdown theano GPU so we can use multiprocessing
theano.sandbox.cuda.gpu_shutdown()
segment_identifier = csi.FixedWindowAroundPeaks(
    slidingWindowForMaxSize=args.seqlet_size,
    flankToExpandAroundPeakSize=args.seqlet_flank,
    excludePeaksWithinWindow=args.seqlet_size + args.seqlet_flank,
    ratioToTopPeakToInclude=DEEPLIFT_RATIO_TO_TOP_PEAK,
    maxSegments=DEEPLIFT_MAX_SEQLETS)

logger.info('Finding seqlets...')
grammars, grammar_indices = csi.getSeqlets(
    rawDeepLIFTContribs=deepLIFT_scores[args.input_layer],
    indicesToGetSeqletsOn=None,
    outputsBeforeActivation=None,
    activation=None,
    thresholdProb=1.0,
    segmentIdentifier=segment_identifier,
    numThreads=NUM_THREADS,
    secondsBetweenUpdates=6,
    includeNeg=True)

# augment seqlets with sequence, deepLIFT, and gradient tracks
deepLIFT_track = deeplift_trackname(args.input_layer)
gradient_track = gradient_trackname(args.input_layer)
sequence_track = SEQUENCE_TRACKNAME

csi.augmentSeqletsWithData(
    grammars,
    fullDataArr=np.squeeze(deepLIFT_scores[args.input_layer], axis=1),
    keyName=deepLIFT_track,
    revCompFunc=csi.dnaRevCompFunc,
    pseudocount=0)

csi.augmentSeqletsWithData(
    grammars,
    fullDataArr=np.squeeze(gradients_only[args.input_layer], axis=1),
    keyName=gradient_track,
    revCompFunc=csi.dnaRevCompFunc,
    pseudocount=0)

for field in data['input_data']._fieldnames:
    field_data = getattr(data['input_data'], field)
    pseudocount = 0.25 if field == 'sequence' else None
    rev_comp_func = (csi.dnaRevCompFunc if field == 'sequence'
                     else csi.reverseFunc)
    csi.augmentSeqletsWithData(
        grammars,
        fullDataArr=expand_dims_blob(field_data, 3),
        keyName=field,
        pseudocount=pseudocount,
        revCompFunc=rev_comp_func,
        indicesToSubset=indices_to_use)

# Get distribution of seqlet scores
seqlet_deepLIFT_scores = [
    np.sum(grammar.summedDataTracks[deepLIFT_track].data[:, args.seqlet_flank:-args.seqlet_flank])
    for grammar in grammars
]

sns.distplot(seqlet_deepLIFT_scores)
plt.savefig('{}-seqlet_deeplift_scores.svg'.format(output_prefix))
plt.close()

# Cluster seqlets by gradients
logger.info('Initial k-means clustering...')
subset_indices = np.random.choice(np.arange(len(grammars)), size=1000)
grammars_subset = np.array(itemgetter(*subset_indices)(grammars))

grammars_corr_mat = csi.getCorrelationMatrix(
    grammars_subset,
    subtracksToInclude=[gradient_track],
    normaliseFunc=util.CROSSC_NORMFUNC.meanAndTwoNorm,
    accountForRevComp=True,
    numThreads=NUM_THREADS,
    secondsBetweenUpdates=6,
    xcorBatchSize=XCOR_GPU_BATCHSIZE)

max_grammars_cc = np.max(grammars_corr_mat, axis=1)
len(max_grammars_cc)
# sns.distplot(max_grammars_cc)

# Next, cluster seqLets using gradient-based cross correlation features.
#clusterer = MiniBatchKMeans(n_clusters=KMEANS_NUM_CLUSTERS,
#                            n_init=KMEANS_INITS)
#labels = clusterer.fit_predict(grammars_corr_mat)
max_cc = grammars_corr_mat.max()
min_cc = grammars_corr_mat.min()
logger.info('Min cc: {}, Max cc: {}'.format(min_cc, max_cc))
corr_mat_diag = grammars_corr_mat.diagonal()
logger.info('Diagonal min cc: {}, max cc: {}'.format(corr_mat_diag.min(),
                                                     corr_mat_diag.max()))
assert 0 <= min_cc and max_cc <= 1

grammars_corr_mat *= -1
grammars_corr_mat += 1
np.fill_diagonal(grammars_corr_mat, 0)
try:
    labels, _, _ = kmedoids(grammars_corr_mat, n_clusters=KMEANS_NUM_CLUSTERS,
                            n_init=KMEANS_INITS)
    #linkage_matrix = linkage(grammars_corr_mat, method='average')
    #labels = fcluster(linkage_matrix, KMEANS_NUM_CLUSTERS, criterion='maxclust')
except:
    np.save('corrs.npy', grammars_corr_mat)

# merge seqlets
#merged_grammars = csi.createMergedGrammars(labels,
merged_grammars, original_shifts = csi.merge_grammars(
        labels,
        grammars_subset,
        subtracksToInclude=[gradient_track],
        normaliseFunc=util.CROSSC_NORMFUNC.meanAndTwoNorm,
        accountForRevComp=True)

import IPython; IPython.embed()

# trim grammars
trimmingFunc = csi.TrimArrayColumnsToNumUnderlyingObs(TRIM_FRAC)
merged_grammars = csi.adjustGrammarsUsingTrimmingCriterion(
    merged_grammars, trimmingFunc=trimmingFunc)


# perform agglomorative clustering
logger.info('Agglomerative clustering...')
agglomerated_grammars, agglomerated_indices = agglomerative_clustering(
    merged_grammars, gradient_track, cc_threshold=0.9)


if args.save:
    logger.info('Saving clusters to disk...')
    with open('{}-clustered-seqlets.pkl'.format(output_prefix), 'wb') as fp:
        pickle.dump(agglomerated_grammars, fp, pickle.HIGHEST_PROTOCOL)
        pickle.dump(agglomerated_indices, fp, pickle.HIGHEST_PROTOCOL)

merged_grammars_cc = csi.getCorrelationMatrix(
    merged_grammars.values(),
    subtracksToInclude=[gradient_track],
    accountForRevComp=True,
    numThreads=1,
    secondsBetweenUpdates=6,
    xcorBatchSize=None,
    smallerPerPosNormFuncs=[util.PERPOS_NORMFUNC.oneOverTwoNorm],
    largerPerPosNormFuncs=[util.PERPOS_NORMFUNC.oneOverTwoNorm])

plot_clustering_heatmap(merged_grammars_cc, agglomerated_indices)
plt.savefig('{}-agglomerative_clustering_heatmap.svg'.format(
    output_prefix))
plt.close()

for grammar in agglomerated_grammars:
    convert_pwm_to_bits(grammar.getNormalisedDataTrack('sequence'))

plot_tracks = [
    deeplift_trackname(args.input_layer),
    SEQUENCE_TRACKNAME,
    gradient_trackname(args.input_layer)
] + [field for field in data['input_data']._fieldnames if field != 'sequence']

set_plot_style()

for idx in range(len(agglomerated_grammars)):
    csi.printGrammarWithIdx(agglomerated_grammars,
                            idx=idx,
                            trackNamesToPrint=plot_tracks,
                            heightPerTrack=3)
    plt.savefig('{}-grammar-{}.svg'.format(
        output_prefix, idx))
    plt.close()


with open('{}-known-motifs-gradients.txt'.format(output_prefix), 'w') as fp:
    _stdout = sys.stdout
    sys.stdout = fp
    csi.compareToKnownMotifs({idx: grammar
                              for idx, grammar in
                              enumerate(agglomerated_grammars)},
                             trackNameForComparison=gradient_track,
                             match_negatives=False)
    sys.stdout = _stdout

with open('{}-known-motifs-deepLIFT.txt'.format(output_prefix), 'w') as fp:
    _stdout = sys.stdout
    sys.stdout = fp
    csi.compareToKnownMotifs({idx: grammar
                              for idx, grammar in
                              enumerate(agglomerated_grammars)},
                             trackNameForComparison=deepLIFT_track,
                             match_negatives=False)
    sys.stdout = _stdout
