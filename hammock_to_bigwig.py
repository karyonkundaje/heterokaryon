#converts hammock files to bigwig files 
import argparse
import gzip
from pysam import tabix_index
import os 
import pdb 

def parse_args(): 
    parser=argparse.ArgumentParser(description="convert a file in hammock format to a file in bigwig format")
    parser.add_argument("--hammock_file",help="input hammock file, can be in .gz format",required=True)
    parser.add_argument("--genome",help="chrom sizes file", required=True)
    parser.add_argument("--output_prefix",help="name of output file name",required=True) 
    return parser.parse_args() 


def hammock_to_bedgraph_for_line(hammock_line): 
    tokens=hammock_line.split('\t') 
    chrom=tokens[0] 
    startpos=tokens[1] 
    endpos=tokens[2] 
    qcatvals=tokens[-1].split('[')
    maxval=0 
    for val in qcatvals: 
        if (val.endswith(']')) or (val.endswith(',')) or (val.endswith('\n')):
            val=float(val.split(',')[0])
            if abs(val) > maxval: 
                maxval=val 
    if abs(maxval)<1e-6: 
        return None
    else: 
        return str(chrom)+'\t'+str(startpos)+'\t'+str(endpos)+'\t'+str(maxval)+'\n'
            

def main(): 
    args=parse_args() 
    hammock_file=args.hammock_file 
    genome=args.genome
    output_prefix=args.output_prefix
    output_bedgraph=open(output_prefix+'.bedGraph','w') 

    #we will read & write chunks of the file (to avoid write statements for every line) 
    bedgraph_queue="" 
    lines_queued=0
    max_queue=1000000

    if hammock_file.endswith('.gz'): 
        #unzip the file! 
        input_data=gzip.open(hammock_file,'r') 
    else: 
        input_data=open(hammock_file,'r') 
    total=0 
    nextline=input_data.readline() 
    while nextline!="":
        total+=1 
        if total%1000000==0: 
            print str(total)+"lines processed" 
        #flush the queue if necessary 
        if lines_queued > max_queue: 
            #flush! 
            output_bedgraph.write(bedgraph_queue) 
            bedgraph_queue="" 
            lines_queued=0 
        #process the line 
        bedgraph_entry=hammock_to_bedgraph_for_line(nextline)
        if bedgraph_entry!=None: 
            bedgraph_queue=bedgraph_queue+bedgraph_entry 
            lines_queued+=1 
        #read the next line 
        nextline=input_data.readline() 
    #flush whatever has been queued up 
    output_bedgraph.write(bedgraph_queue)     
    #convert the bedgraph to a bigwig file 
    cmds=['bedGraphToBigWig',
          output_prefix+'.bedGraph',
          genome,
          output_prefix+'.bigWig']
    os.system(' '.join(cmds))
    #bedgraph_to_bigwig(output_prefix+".bedgraph",genome,output_prefix+".bigWig")
        
if __name__ == '__main__': 
    main() 
